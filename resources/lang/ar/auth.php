<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'failed' => 'لا تتطابق بيانات الاعتماد هذه مع سجلاتنا',
    'deleted' => 'تم حذف حسابك بواسطة المشرف. اتصل بالمسؤول!',
    'throttle' => 'هناك عدد كبير جدا من محاولات تسجيل الدخول. الرجاء إعادة المحاولة لاحقا',

];
