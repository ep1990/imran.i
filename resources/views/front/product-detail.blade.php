@extends('front.layouts.app')

@section('css')

<style>
    .card {
        height: 400px;
        margin-top: 20px !important;
        position: relative;
        display: -webkit-box;
        display: -ms-flexbox;
        display: flex;
        -webkit-box-orient: vertical;
        -webkit-box-direction: normal;
        -ms-flex-direction: column;
        flex-direction: column;
        min-width: 0;
        word-wrap: break-word;
        background-color: #fff;
        background-clip: border-box;
        border: none;
        border-radius: 0.25rem;
        position: relative;
        display: -webkit-box;
        display: -ms-flexbox;
        display: flex;
        -webkit-box-orient: vertical;
        -webkit-box-direction: normal;
        -ms-flex-direction: column;
        flex-direction: column;
        min-width: 0;
        word-wrap: break-word;
        background-color: #fff;
        background-clip: border-box;
        border: none;
        border-radius: 0.25rem;
    }
</style>

@endsection

@section('content')

<main class="py-4">
    <div class="container">
        <div class="row justify-content-center">
                <div class="col-md-12">
                    <div class="row">
                        <div class="col-md-6">
                            <img src="{{ asset($product->image) }}">
                        </div>
                        <div class="col-md-6">
                            <h3>{{ $product->title }}</h3>
                            <p>{{ $product->description }}</p>
                            <form method="post" action="{{ route('front.cart.add') }}" class="quantity-car-btn mt-4 d-flex flex-wrap flex-md-nowrap">
                                @csrf()
                                <input type="hidden" value="{{$product->id}}" name="product_id">
                                <div class="input-group increment-height mb-3 mb-xl-0 d-flex align-items-center mr-2">
                                <button type="button" class="btn quant-btn increment-height " onclick="addRemove('minus')">
                                -
                                </button>
                                <input type="text" name="quantity" maxlength="3" id="quantity" class="form-control input-number text-center increment-height" value="1">
                                <button type="button" class="btn quant-btn increment-height " onclick="addRemove('add')"  data-type="plus" data-field="">
                                +
                                </button>
                                </div>
                    
                                <div class="user-profile-detail-mt">
                                    <input type="submit" class="btn btn-primary profile-btn" value="{{__('add to cart')}}"> 
                                </div>
                            </form>
                            @if(session()->has('error'))
                                <br>
                                <p class="alert alert-danger">{{ session('error') }}</p>
                            @endif
                        </div>
                    </div>
                </div>      
        </div>
    </div>
</main>

@endsection

@section('js')
    <script>
        function addRemove(value)
        {
        if(value == 'add'){
            if($('#quantity').val() >= 100){
                $('#quantity').val(100);
                return false;
            }
            $('#quantity').val(parseInt($('#quantity').val())+parseInt(1));
        }

        if(value == 'minus'){
            if($('#quantity').val() <= 1){
                $('#quantity').val(1);
                return false;
            }
            $('#quantity').val($('#quantity').val()-1);
        }
        }
    </script>
@endsection