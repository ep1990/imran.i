@extends('front.layouts.app')

@section('css')

<style>
    .card {
        height: 400px;
        margin-top: 20px !important;
        position: relative;
        display: -webkit-box;
        display: -ms-flexbox;
        display: flex;
        -webkit-box-orient: vertical;
        -webkit-box-direction: normal;
        -ms-flex-direction: column;
        flex-direction: column;
        min-width: 0;
        word-wrap: break-word;
        background-color: #fff;
        background-clip: border-box;
        border: none;
        border-radius: 0.25rem;
        position: relative;
        display: -webkit-box;
        display: -ms-flexbox;
        display: flex;
        -webkit-box-orient: vertical;
        -webkit-box-direction: normal;
        -ms-flex-direction: column;
        flex-direction: column;
        min-width: 0;
        word-wrap: break-word;
        background-color: #fff;
        background-clip: border-box;
        border: none;
        border-radius: 0.25rem;
    }
</style>

@endsection

@section('content')

<main class="py-4">
    <div class="container">
        <div class="row justify-content-center">
            @forelse($products as $product)
                <div class="col-md-3">
                    <div class="card" style="margin-top:20px">
                        <div class="image-box d-flex align-items-center justify-content-center">
                            <img src="{{ asset($product->image) }}" class="img-fluid" alt="...">
                        </div>
                        <div class="card-body text-center">
                            <h5 class="card-title ">{{ $product->title }}</h5>
                            <p class="card-text text-uppercase ">${{ $product->price }}</p>
                            <a href="{{ route('front.product.detail', $product->slug) }}" class="btn btn-primary text-capitalize add-to-cart">View Detail</a>
                        </div>
                        <div class="card-bottom-bg">
                        </div>
                    </div>
                </div>
            @empty
            <div class="col-md-3 alert alert-danger">No record found</div>
            @endforelse        
        </div>
        <br><br>
        {{ $products->links() }}
    </div>
</main>

@endsection