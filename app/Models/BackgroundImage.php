<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class BackgroundImage extends Model
{
    use \Illuminate\Database\Eloquent\SoftDeletes;
    use \App\Models\CommonModelFunctions;
    use CommonFunctions;
    protected $dateFormat = 'U';
    public static $snakeAttributes = false;


    protected $fillable = [
        'title->en',
        'title->ar',
        'image'
    ];
    protected $casts = [
        'title' => 'array',
        'created_at' => 'int',
        'updated_at' => 'int'
    ];

    public function kiosk()
    {
        return $this->hasMany(User::class);
    }

    // public function areas(){
    //     return $this->hasMany(Area::class);
    // }

}