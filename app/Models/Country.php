<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Country extends Model
{
    use \Illuminate\Database\Eloquent\SoftDeletes;
    use \App\Models\CommonModelFunctions;
    use CommonFunctions;
    protected $dateFormat = 'U';
    public static $snakeAttributes = false;


    protected $fillable = [
        'parent_id'
    ];
    protected $casts = [
        'created_at' => 'int',
        'updated_at' => 'int'
    ];

    public function languages(){
        return $this->belongsToMany('App\Models\Language')->withPivot('name');
    }

    public function cities(){
        return $this->hasMany(Country::class, 'parent_id');
    }

}
