<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;

class FaqLanguage extends Model
{
    use \App\Models\CommonModelFunctions;
    protected $table = 'faq_language';
    public $incrementing = false;
    public $timestamps = false;
    protected $dateFormat = 'U';
    public static $snakeAttributes = false;

    protected $casts = [
        'language_id' => 'int',
        'faq_id' => 'int'
    ];

    protected $fillable = [
        'language_id',
        'faq_id',
        'question',
        'answer'
    ];

    public function faq()
    {
        return $this->belongsTo(\App\Models\Faq::class);
    }

    public function language()
    {
        return $this->belongsTo(\App\Models\Language::class);
    }
}
