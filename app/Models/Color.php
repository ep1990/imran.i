<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Color extends Model
{
    use \Illuminate\Database\Eloquent\SoftDeletes;
    use \App\Models\CommonModelFunctions;
    use CommonFunctions;
    protected $dateFormat = 'U';
    public static $snakeAttributes = false;


    protected $fillable = [
        'title->en',
        'title->ar',
        'code',
        'image'
    ];
    protected $casts = [
        'title' => 'array',
        'created_at' => 'int',
        'updated_at' => 'int'
    ];

    public function kioskImages()
    {
        return $this->hasMany(KioskImage::class);
    }

    public function getImageAttribute() {
        if (!empty($this->attributes['image'])){
            return url($this->attributes['image']);
        }
        return url('images/colors.jpg');
    }

    // public function areas(){
    //     return $this->hasMany(Area::class);
    // }

}