<?php

/**
 * Created by Reliese Model.
 * Date: Fri, 20 Apr 2018 05:47:07 +0000.
 */

namespace App\Models;

//use Reliese\Database\Eloquent\Model as Eloquent;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Page
 * 
 * @property int $id
 * @property string $slug
 * @property string $image
 * @property int $created_at
 * @property int $updated_at
 * @property string $deleted_at
 * 
 * @property \Illuminate\Database\Eloquent\Collection $languages
 *
 * @package App\Models
 */
class Page extends Model
{
	use \Illuminate\Database\Eloquent\SoftDeletes;
	use \App\Models\CommonModelFunctions;
	use CommonFunctions;
	protected $dateFormat = 'U';
	public static $snakeAttributes = false;

	protected $hidden = [
	  'created_at', 'updated_at', 'deleted_at'
    ];

	protected $casts = [
		'created_at' => 'int',
		'updated_at' => 'int',
		'title' => 'array',
		'content' => 'array'
	];

	protected $fillable = [
		'slug',
		'image',
        'page_type',
		'icon',
		'title->en',
		'title->ar',
		'content->en',
		'content->ar',
		'short_descripiotn->en',
		'short_descripiotn->ar'
	];
    public function getImageAttribute()
    {
        if (empty($this->attributes['image']))
        {
            return url('images/category.jpg');
        }
        return url( $this->attributes['image']);
    }
}
