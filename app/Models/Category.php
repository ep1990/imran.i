<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;


class Category extends Model
{
    use \Illuminate\Database\Eloquent\SoftDeletes;
    use \App\Models\CommonModelFunctions;
    use CommonFunctions;
    protected $dateFormat = 'U';
    public static $snakeAttributes = false;


    protected $fillable = [
        'parent_id','image','title->en','title->ar'
    ];
    protected $casts = [
        'title' => 'array',
        'created_at' => 'int',
        'updated_at' => 'int'
    ];

    public function products(){
        return $this->belongsToMany('App\Models\Product');
    }

    protected function getImageAttribute() {
        if (!empty($this->attributes['image'])){
            return url($this->attributes['image']);
        }
        return url('images/category.jpg');
    }

    public function attributes(){
        return $this->belongsToMany(Attribute::class);
    }
    
    public function categories(){
        return $this->belongsTo(Category::class,'parent_id');
    }

    public function subCategories(){
        return $this->hasMany(Category::class, 'parent_id');
    }

    public function users()
    {
        return $this->belongsToMany(User::class);
    }

    public function languages()
    {
        return $this->belongsToMany(Language::class);
    }


}
