<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AgencyImage extends Model
{
    use \Illuminate\Database\Eloquent\SoftDeletes;
    use \App\Models\CommonModelFunctions;
    protected $dateFormat = 'U';
    public static $snakeAttributes = false;
    protected $casts = [
        'agency_id' => 'int',
        'created_at' => 'int',
        'updated_at' => 'int'
    ];
    protected $fillable = [
        'agency_id',
        'image',
        'default_image'
    ];
    public function Agency()
    {
        return $this->belongsTo(\App\Models\Agency::class);
    }
    public function getImageAttribute()
    {
        if (!empty($this->attributes['image']))
        {
            return  $this->attributes['image'];
        }
        return url(env('PRODUCT_DEFAULT_DETAIL_IMAGE'));
    }
}
