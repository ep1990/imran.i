<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use stdClass;

class KioskImage extends Model
{
    use \Illuminate\Database\Eloquent\SoftDeletes;
    use \App\Models\CommonModelFunctions;
    use CommonFunctions;
    protected $dateFormat = 'U';
    public static $snakeAttributes = false;
    // public $timestamp = false;


    protected $fillable = [
        'kiosk_id',
        'color_id',
        'image',
        'is_default'
    ];
    protected $casts = [
        'title' => 'array',
        'created_at' => 'int',
        'updated_at' => 'int'
    ];

    public function getColorIdAttribute($value)
    {
        if($value == NULL)
        {
            return new \stdClass;
        }
        return $value;
    }

    public function kiosk(){
        return $this->belongsTo(Kiosk::class);
    }

    public function color()
    {
       return $this->belongsTo(Color::class);
    }

    public function user()
    {
        return $this->hasMany(User::class);
    }

}