<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Kiosk extends Model
{
    use \Illuminate\Database\Eloquent\SoftDeletes;
    use \App\Models\CommonModelFunctions;
    use CommonFunctions;
    protected $dateFormat = 'U';
    public static $snakeAttributes = false;


    protected $fillable = [
        'title->en',
        'title->ar',
    ];
    protected $casts = [
        'title' => 'array',
        'created_at' => 'int',
        'updated_at' => 'int'
    ];

    public function kioskImages(){
        return $this->hasMany(KioskImage::class);
    }

    public function kioskDefaultImage()
    {
        return $this->hasOne(KioskImage::class)->where('is_default',1);
    }

}