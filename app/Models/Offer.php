<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Offer extends Model
{
    use \Illuminate\Database\Eloquent\SoftDeletes;
    use \App\Models\CommonModelFunctions;
    protected $dateFormat = 'U';
    public static $snakeAttributes = false;
    protected $casts = [
        'price' => 'float',
        'created_at' => 'int',
        'updated_at' => 'int',
    ];

    protected $fillable = [
        'price',
        'image',
    ];
    public function languages()
    {
        return $this->belongsToMany(\App\Models\Language::class)
            ->withPivot('title', 'short_description', 'description');
    }
}
