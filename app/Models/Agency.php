<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Agency extends Model
{
    use \Illuminate\Database\Eloquent\SoftDeletes;
    use \App\Models\CommonModelFunctions;
    protected $dateFormat = 'U';
    public static $snakeAttributes = false;
    protected $fillable = [
        'country_id',
        'image',
        'city_id',
        'email',
        'phone',
        'landline',
        'parent_id',
        'agency_location',
        'latitude',
        'longitude'
    ];

    public function getImageAttribute()
    {
        if (empty($this->attributes['image']))
        {
            return 'images/productDetail.jpg';
        }
        return $this->attributes['image'];
    }
    public function languages(){
        return $this->belongsToMany('App\Models\Language')->withPivot('name','long_description');
    }


    public function country(){
        return $this->belongsTo(Country::class);
    }
    public function city(){
        return $this->belongsTo(Country::class);
    }
    public function images(){
        return $this->hasMany(AgencyImage::class);
    }

}
