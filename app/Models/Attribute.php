<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Attribute extends Model
{
    use \Illuminate\Database\Eloquent\SoftDeletes;
    use \App\Models\CommonModelFunctions;
    use CommonFunctions;
    protected $dateFormat = 'U';
    public static $snakeAttributes = false;


    protected $fillable = [
        'parent_id','title->en','title->ar','multiple'
    ];
    protected $casts = [
        'title' => 'array',
        'created_at' => 'int',
        'updated_at' => 'int',
        'deleted_at'
    ];
    protected $hidden = ['pivot'];


    public function subAttributes(){
        return $this->hasMany(Attribute::class, 'parent_id');
    }
    public function categories(){
        return $this->belongsToMany(Category::class);
    }
}
