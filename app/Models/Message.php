<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Message extends Model
{
    protected $dateFormat = 'U';
    protected $fillable = ['conversation_id', 'message_type', 'message', 'sent_by', 'file'];

    public function conversation()
    {
        return $this->belongsTo(Conversation::class);
    }

    public function getFileAttribute($value)
    {
        return url($value);
    }
}
