<?php

use App\Http\Libraries\ResponseBuilder;
use App\Models\City;
use App\Models\CityLanguage;
use App\Models\Notification;
use Carbon\Carbon;

function imageUrl($path, $width = NULL, $height = NULL, $quality = NULL, $crop = NULL)
{
    if (!$width && !$height) {
        $url = env('IMAGE_URL') . $path;
    } else {
        $url = url('/') . '/images/timthumb.php?src=' . env('IMAGE_URL') . $path;
        if (isset($width)) {
            $url .= '&w=' . $width;
        }
        if (isset($height) && $height > 0) {
            $url .= '&h=' . $height;
        }
        if (isset($crop)) {
            $url .= "&zc=" . $crop;
        } else {
            $url .= "&zc=1";
        }
        if (isset($quality)) {
            $url .= '&q=' . $quality . '&s=0';
        } else {
            $url .= '&q=95&s=0';
        }
    }
    return $url;
}


function responseBuilder()
{
    $responseBuilder = new ResponseBuilder();
    return $responseBuilder;
}

function getConversionRate()
{
    if (Cache::has('AED')){
        $rate = Cache::get('AED');
    }
    else{
//        $swapEURToUSD = \Swap::latest('EUR/USD');
//        $swapEURToAED = \Swap::latest('EUR/AED');
//        // rate of 1 USD in EUR
//        $rateUSDToEUR = 1 / $swapEURToUSD->getValue();
//        // rate of 1 AED in EUR
//        $rateAEDToEUR = 1 / $swapEURToAED->getValue();
////                $rate  = round(((1 / $rateUSDToEUR) * $rateAEDToEUR), 2);
        $rate = 3.67;
        Cache::put('AED', $rate, Carbon::now()->addMinutes(60));
    }
    return $rate;

}

function getPriceObject($productPrice)
{
    $rate =getConversionRate();
    $price = new \stdClass();
    $price->usd = new \stdClass();
    $price->usd->amount = round($productPrice / $rate ,2);
    $price->usd->currency = 'USD';
    $price->aed = new \stdClass();
    $price->aed->amount = $productPrice;
    $price->aed->currency = 'AED';
    return $price;
}

function getDiscountPriceObject($productPrice, $dicsount)
{
    $rate =getConversionRate();
    $price = new \stdClass();
    $price->usd = new \stdClass();
    $amount = round($productPrice / $rate ,2)*$dicsount/100;
    $price->usd->amount = round($productPrice / $rate ,2)-$amount;
    $price->usd->currency = 'USD';
    $price->aed = new \stdClass();
    $amount = $productPrice*$dicsount/100;
    $price->aed->amount = $productPrice-$amount;
    $price->aed->currency = 'AED';
    return $price;
}

function getAmountString($priceObject , $currency,$quantity=null)
{
    if($quantity==null)
    {
        $price = round($priceObject->{strtolower($currency)}->amount,2);
        $symbol = $priceObject->{strtolower($currency)}->currency;
        return $symbol .' '. $price;
    }
    elseif($quantity!=null)
    {

        $price = round($priceObject->{strtolower($currency)}->amount *$quantity ,2);
        $symbol = $priceObject->{strtolower($currency)}->amount;
        return $symbol .' '. $price;
    }


}




    function trim_text($input) {
        $middle = ceil(strlen($input) / 2);
        $middle_space = strpos($input, " ", $middle - 1);
            $first_half = substr($input, 0, $middle_space);
            $second_half = substr($input, $middle_space);
        return  $first_half.'@'.$second_half;
    }

/**
 * Set session alert / flashdata
 * @param string $type Alert type
 * @param string $message Alert message
 */
function set_alert($type, $message)
{
    session()->flash('alert_type', $type);
    session()->flash('alert_message', $message);
}



function calcualateDistance($lat1, $lon1, $lat2, $lon2, $unit)
{
    if (($lat1 == $lat2) && ($lon1 == $lon2)) {
        return 0;
    } else {
        $theta = $lon1 - $lon2;
        $dist = sin(deg2rad($lat1)) * sin(deg2rad($lat2)) + cos(deg2rad($lat1)) * cos(deg2rad($lat2)) * cos(deg2rad($theta));
        $dist = acos($dist);
        $dist = rad2deg($dist);
        $miles = $dist * 60 * 1.1515;
        $unit = strtoupper($unit);
        if ($unit == "K") {
            return ($miles * 1.609344);
        } else if ($unit == "N") {
            return ($miles * 0.8684);
        } else {
            return $miles;
        }
    }
}
function getStarRating($value)
{
    $star_rate = $value;
    if ($value > 0  && $value < 0.5)
    {
        $star_rate = 0.5;
    }
    if ($value > 0.5 && $value < 1)
    {
        $star_rate = 1;
    }
    if ($value > 1 && $value < 1.5)
    {
        $star_rate = 1.5;
    }
    if ($value > 1.5 && $value < 2)
    {
        $star_rate = 2;
    }
    if ($value > 2 && $value < 2.5)
    {
        $star_rate = 2.5;
    }
    if ($value > 2.5 && $value < 3)
    {
        $star_rate = 3;
    }
    if ($value > 3 && $value < 3.5)
    {
        $star_rate = 3.5;
    }
    if ($value > 3.5 && $value < 4)
    {
        $star_rate = 4;
    }
    if ($value > 4 && $value < 4.5)
    {
        $star_rate = 4.5;
    }
    if ($value > 5)
    {
        $star_rate = 5;
    }
    return $star_rate;
}

function pluckPackagesValue($availablePackages)
{
    $packges = [];
    foreach ($availablePackages as $key => $availablePackage) {
        $packges[$availablePackage->id] = $availablePackage->package->translation->title . ' ( views ' .$availablePackage->package->views . ' )' ;
    }
    return $packges;
}



function deleteImage($path = ''){
    unlink(url($path));
}


function createNotification($receiver, $sender_id, $action, array $title = ['en' => '', 'ar'=> ''], array $description=['en' => '', 'ar'=> ''], $extras='')
{
    if ($receiver->is_notification == 1) {
        Notification::create([
            'receiver_id' => $receiver->id,
            'sender_id' => $sender_id,
            'fcm_token' => $receiver->fcm_token,
            'action' => $action,
            'title->en' => $title['en'],
            'title->ar' => $title['ar'],
            'description->en' => $description['en'],
            'description->ar' => $description['ar'],
            'is_seen' => 0,
            'is_read' => 0,
            'extras' => $extras
        ]);
    }
}