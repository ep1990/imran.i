<?php



namespace App\Http\Services\users;




use App\Models\User;



use Illuminate\Foundation\Auth\RegistersUsers;


use Tymon\JWTAuth\Facades\JWTAuth;



class UserService

{

    use RegistersUsers;

    private $reqData;

    private $user;



    /**

     * UserService constructor.

     * @param $data

     * @param null $user

     */



    public function __construct($reqData, $user = null)

    {

        $this->reqData = $reqData;

        $this->user = $user;

    }



    public function register()

    {

        $data['email'] =  $this->reqData->email;
        if($this->reqData->user_type == 'user'){
            $data['first_name'] = $this->reqData->first_name;
            $data['last_name'] = $this->reqData->last_name;
            $data['is_supplier'] = 0;
        }

        if($this->reqData->user_type == 'supplier'){
            $data['supplier_name'] = $this->reqData->supplier_name;
            $data['is_supplier'] = 1;
        }

        $data['password'] = bcrypt($this->reqData->password);
        $user = User::create($data);


        return $user;

    }

}