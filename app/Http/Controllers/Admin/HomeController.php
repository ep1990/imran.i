<?php

namespace App\Http\Controllers\Admin;

use App\Models\Product;
use Illuminate\Foundation\Auth\User;
use Illuminate\Http\Request;
use App\Traits\Administrators;
use App\Http\Controllers\Controller;
use Symfony\Component\Finder\SplFileInfo;
use App\Http\Libraries\Uploader;

class HomeController extends Controller {


    public function __construct() {
        parent::__construct('adminData', 'admin');
    }

    public function index() {
        $products = Product::latest('created_at')->with('images')->paginate(8);
        return view('admin.home', ['products' => $products]);
    }

    public function uploadImage(Request $request){
        $this->validate(request(),['image' => 'required | image']);
        $imageUploadedPath = '';
        if ($request->hasFile('image')) {
            $uploader = new Uploader('image');
            if ($uploader->isValidFile()) {
                $uploader->upload('media', $uploader->fileName);
                if ($uploader->isUploaded()) {
                    $imageUploadedPath = $uploader->getUploadedPath();
                }
            }
            if (!$uploader->isUploaded()) {
                return 0;
            }
        }
        return response(['status_code' => '200', 'message' => 'Image uploaded successfully.', 'data' => $imageUploadedPath]);
    }

    public function deleteImage(Request $request){
        unlink(public_path($request->path));
    }
}
