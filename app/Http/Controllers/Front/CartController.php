<?php

namespace App\Http\Controllers\Front;

use App\Models\Attribute;
use App\Models\Cart;
use App\Models\Coupon;
use App\Models\Product;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use phpDocumentor\Reflection\Types\Null_;
use Carbon\Carbon;
use App\Traits\CartTrait;


class CartController extends Controller
{
    use CartTrait;


    public function __construct() {
        $this->middleware('auth');
        parent::__construct();
    }
    
    public function add(Request $request){
        $this->validate(request(), ['product_id' => 'required | exists:products,id', 'quantity' => 'required | integer | min:1']);
        $cart = $this->addToCart($request);
        if($cart == 'supplier'){
            return back()->with('error', __('Supplier cannot use add to cart functionality'));
        }
        if($cart == 'out of stock'){
            return back()->with('error', __('Product is out of stock'));
        }
        if($cart == 'greater than stock'){
            return back()->with('error', __('Required quantity is greater than stock'));
        }
        if($cart == 'success'){
            return  redirect(route('front.cart.index'));
        }
        return back()->with('error', __('Something went wrong'));     
    }

    public  function index(){
        return view('front.cart');
    }


    public function getCart()
    {
        $cartData = $this->cartIndex();
        return json_encode($cartData);
    }



    public function updateCart(Request $request){
        $request->validate([
            'cart_id' => 'required|exists:carts,id',
            'quantity' => 'required|numeric'
        ]);
        if($request->quantity < 1)
        {
            return responseBuilder()->error(__('Please enter quantity greater than 0'));
        }
        $cart = $this->updateCartTrait($request, true);
        if ($cart == "please change quantity"){
            return responseBuilder()->error(__('Please change quantity'));
        }
        if ($cart == "greater than stock"){
            return responseBuilder()->error(__('Your Quantity Is Greater Than Stock'));
        }
        if ($cart == "success"){
            return responseBuilder()->success(__('success'));
        }
        return responseBuilder()->error(__("Something went wrong"));

    }

    public function deleteCart($id){
        $cart = $this->deleteCartTrait($id, true);
        if($cart == 'success'){
            $cartData = $this->cartIndex(true);
            return json_encode($cartData);
        }
    }

}
