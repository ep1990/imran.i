<?php

namespace App\Http\Controllers\Front\Auth;

use App\Http\Services\users\UserService;
use App\Models\City;
use App\Models\CityLanguage;
use App\Models\Page;
use App\Models\User;
use Gloudemans\Shoppingcart\Cart;
use Illuminate\Http\Request;
use Tymon\JWTAuth\Facades\JWTAuth;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Validation\ValidationException;

class LoginController extends Controller
{
    /*
      |--------------------------------------------------------------------------
      | Login Controller
      |--------------------------------------------------------------------------
      |
      | This controller handles authenticating users for the application and
      | redirecting them to your home screen. The controller uses a trait
      | to conveniently provide its functionality to your applications.
      |
     */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {

        $this->middleware('guest', ['except' => 'logout']);
        $this->breadcrumbTitle = 'Sitemap';
        parent::__construct();
    }

    protected function attemptLogin(Request $request)
    {
        $credentials = $request->only(['email', 'password']);
        if (auth()->attempt($credentials, $request->get('remember'))) {
            set_alert('success', __('Login Successfully'));
            $user = auth()->user();
            $user['token'] = JWTAuth::fromUser($user);
            $cartCount = \App\Models\Cart::where('user_id',auth()->user()->id)->count();
            session()->put('USER_DATA', $user);
            session()->flash('status','Login Successful');
            return redirect(route('front.index'));
        }
        set_alert('danger', __('Please Provide Correct Email Or Password'));
    }

    public function logout(Request $request)
    {

        auth()->logout();
        set_alert('success', __('Logout Successfully!'));
        session()->forget('USER_DATA');
        session()->forget('CART_PRODUCTS');
        session()->forget('cart');
        return redirect('/'.config('app.locale'));
    }
    public function showLoginForm()
    {
        return view('front.login');
    }
    protected function sendFailedLoginResponse(Request $request)
    {
        $user = User::where('email',$request->email)->withTrashed()->first();
        if ($user !== null && $user->deleted_at != null){
            throw ValidationException::withMessages([
            $this->username() => __('Your Account Has Been Deleted! Contact Admin!'),
        ]);
        }
        throw ValidationException::withMessages([
            $this->username() => [trans('auth.failed')],
        ]);
    }
    public function redirectTo() {
        return config('app.locale');
    }




}