<?php

namespace App\Http\Controllers\Front;


use App\Http\Controllers\Controller;
use App\Http\Requests\User\UserRequest;
use App\Http\Requests\ContactUs;
use App\Http\Libraries\Uploader;
use App\Http\Services\users\UserService;
use App\Models\{Category, Page, Product, User,City};
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Traits\{EMails, SuppliersTrait, Products};
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use Tymon\JWTAuth\Facades\JWTAuth;

class IndexController extends Controller {
    public function __construct() {
        parent::__construct();
    }

    
    public function index() {
        $products = Product::latest('created_at')->paginate(8);
        return view('front.index',['products' => $products]);
    }

    public function detail($slug){
        $product = Product::where('slug',$slug)->first();
        return view('front.product-detail',['product' =>  $product]);
    }
    
    

    
}
