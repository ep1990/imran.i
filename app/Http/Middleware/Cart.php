<?php

namespace App\Http\Middleware;

use Closure;

class Cart {

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next) {
        $user = auth()->user();
        if ($user->is_supplier == 1){
            return redirect(route('front.index'));
        }
        return $next($request);
    }

}
