<?php

namespace App\Http\Requests\User;

use Illuminate\Foundation\Http\FormRequest;

class UserRequest extends FormRequest
{


    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'email' => 'required|email|max:255|unique:users',
            'password' => 'required|min:6|alpha_dash|confirmed|max:32',
            'password_confirmation' => 'required|min:6|alpha_dash|max:32',
        ];
        if($this->request->get('user_type') == 'user'){
            $rules['first_name'] = 'required|max:45';
            $rules['last_name'] = 'required|max:45';
        }

        if($this->request->get('user_type') == 'supplier'){
            $rules['supplier_name'] = 'required|max:100';
        }

        return $rules;
    }

    public function messages()
    {
        return [
            'first_name.max'  => 'First name  can be maximum upto 45 characters',
            'last_name.required'  => 'Last name  is required',
            'last_name.max'  => 'Last name  can be maximum upto 45 characters'
    
        ];
    }



}
