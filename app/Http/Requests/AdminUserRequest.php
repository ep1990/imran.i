<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class AdminUserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
//        $user = \Auth::user();
        $rules = [
            'first_name' => 'required|max:32',
            'last_name' => 'required|max:32',
            'user_image' => 'image',
            'phone_number' => ['required','regex:/^([+]|[00]{2})([0-9]|[ -])*/','min:12','max:14'],
            'gender'=> 'required',
            'address'=> 'required',
            'password' => 'nullable|min:6|confirmed|alpha_dash',
            // 'email' => 'required|max:255|email|unique:users,email'
        ];

        if ($this->request->get('user_id') == 0){
//            $rules['user_image'] = 'required|image';
            $rules['email']='required|max:255|email|unique:users,email,NULL,id,deleted_at,NULL';
        }

        return $rules;

    }
}
