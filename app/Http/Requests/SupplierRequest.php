<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class SupplierRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'supplier_name' => 'required|max:100',
            // 'license_expiry_date' => 'required|date|after:'.date('d-m-Y'),
            'phone_number' => ['required','regex:/^([+]|[00]{2})([0-9]|[ -])*/','min:12','max:14'],
            'address'=> 'required',
            'latitude' => 'required',
            'longitude' => 'required',
            'password' => 'nullable|min:6|confirmed|alpha_dash',
            'trade_license' => 'image',
        ];
        if ($this->request->get('supplier_id') == 0){
            $rules['email']='required|max:255|email|unique:users,email,NULL,id,deleted_at,NULL';
        }
        return $rules;

    }

    public function messages()
    {
        return [
            'latitude.required' => 'Please enter valid address',
            'longitude.required' => 'Please enter valid address',
        ];
    }
}
