<?php

namespace App\Http\Requests;

class ContactUs extends MyTechnology {

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize() {
        return true;
    }

    public $errorBag = 'contact_form';
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules() {
        return [
            'last_name' => 'required',
            'email' => 'required|email|regex:/[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,6}/',
            'subject' => 'required',
            'message_text' => 'required',
        ];
    }

    public function messages()
    {
        return [
            'message_text.required' => __('comment field is required'),
            'last_name.required' => __('full name field is required'),
            'subject.required' => __('Subject field is required'),
            'email.required' => __('email field is required'),
            'email.email' => __('email is not a valid email'),
        ];
    }

}
