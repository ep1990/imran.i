<?php

namespace App\Console\Commands;

use App\Models\Language;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\View;

class MyTechnology extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'my-technology:crud {view} {--params=} {--language}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $view = $this->argument('view');
        $model = ucwords(str_singular($view));
        $option = $this->option('params');
        $language = $this->option('language');
        $request = ucwords(camel_case(str_replace('-', ' ', $model)));
        $variable = camel_case(str_singular(str_replace('-', ' ', $view)));
        if (\Route::has('admin.'.$view.'.index')){
            /*
             * MAKING REQUEST
             */
            Artisan::call('make:request', ['name'=>$request.'Request']);
            /*
             * INDEX VIEW PARSING
             */
            $indexView =  View::make('samples.index', ['view'=>$view, 'model'=>$model])->render();
            /*
             * EDIT VIEW PARSING
             */
            $editView =  View::make('samples.edit', ['view'=>$view, 'languages'=>$this->editTabs($language, $view)])->render();
            /*===================================================MAKING FORM FIELDS START=================================================================*/
            $formFields = [];
            $fields = [];
            $params = explode(',', $option);
            foreach ($params as $key => $field) {
                $formFields[$key] = explode(':', $field);
            }
            /*
             * CONTROLLER PARSING
             */
            $controller =  View::make('samples.controller', [
                'controller' => ucwords(camel_case(str_replace('-', ' ', $view))).'Controller',
                'view' => $view,
                'model' => ucwords(camel_case(str_replace('-', ' ', $model))),
                'variable' => $variable,
                'request' => $request.'Request',
                'formFields' => $formFields
            ])->render();

            foreach ($formFields as $key => $field) {
                $isRequired = (in_array('*', $field)) ? "'required'=>'required'" : '';
                $languageField = (in_array('+', $field)) ? 1 : 0;
                $fieldType = explode('.', $field[1]);
                if ($fieldType[0] == 'text') {
                    $fields[$key] = $this->text($field[0], $isRequired, $languageField, $variable);
                }
                if ($fieldType[0] == 'textarea') {
                    $fields[$key] = $this->textArea($field[0], $isRequired, $languageField, $variable);
                }
                if ($fieldType[0] == 'file') {
                    $fields[$key] = $this->file($field[0], $variable);
                }
                if ($fieldType[0] == 'select') {
                    $fields[$key] = $this->select($field[0], $fieldType, $isRequired);
                }
                if ($fieldType[0] == 'radio'){
                    $fields[$key] = $this->radio($field[0], $fieldType, $isRequired);
                }
                if ($fieldType[0] == 'checkbox'){
                    $fields[$key] = $this->checkbox($field[0], $fieldType, $isRequired);
                }
            }
            $formView = View::make('samples.form', ['variable' => $variable, 'view' => $view, 'model' => $model, 'formFields' => $fields])->render();
            /*===================================================MAKING FORM FIELDS END=================================================================*/
            /*
             * DATATABLE JS FILE PARSING
             */
            $js =  View::make('samples.js', ['view'=>$view])->render();
            /*
             * MAKING CONTROLLER
             */
            File::put(base_path('app/Http/Controllers/Admin/'.ucwords(camel_case(str_replace('-', ' ', $view))).'Controller.php'), '<?php '.$controller);
            /*
             * MAKING VIEWS INDEX, EDIT AND FORM
             */
            $file = base_path('resources/views/admin/'.$view);
            File::makeDirectory($file);
            File::put($file."/index.blade.php", $indexView);
            File::put($file."/edit.blade.php", $editView);
            File::put($file."/form.blade.php", $formView);
            /*
             * MAKING JS FILE
             */
            File::put(base_path('public/assets/admin/js/adv_datatables/'.$view.'.js'), $js);

            $this->info('CRUD has been created successfully.');
        }else{
            $this->alert('First create route named as '.$view.'');
        }
    }

    public function text($fieldName, $isRequired, $languageField, $variable){
        $oldDefaultVal = ($languageField == 1)? '$'.$variable.'->translations[$languageId][\''.$fieldName.'\']':''.'$'.$variable.'->'.$fieldName.'';
        $input = '{!! Form::text(\''.$fieldName.'\', old(\''.$fieldName.'\', '.$oldDefaultVal.'), [\'class\' => \'form-control\', \'placeholder\' => \''.ucwords($fieldName).'\', '.$isRequired.']) !!}';
        $field ='<div class="form-group m-form__group row">
                <label for="example-text-input" class="col-3 col-form-label">
                    {!! __(\''.ucwords($fieldName).'\') !!}
                    <span class="text-danger">*</span>
                </label>
                <div class="col-7">
                    '.$input.'
                </div>
              </div>';
        return $field;
    }

    public function textArea($fieldName, $isRequired, $languageField, $variable){
        $oldDefaultVal = ($languageField == 1)? '$'.$variable.'->translations[$languageId][\''.$fieldName.'\']':''.'$'.$variable.'->'.$fieldName.'';
        $input = '{!! Form::textarea(\''.$fieldName.'\', old(\''.$fieldName.'\', '.$oldDefaultVal.'), [\'class\' => \'form-control\', \'placeholder\' => \''.ucwords($fieldName).'\', '.$isRequired.']) !!}';
        $field ='<div class="form-group m-form__group row">
                <label for="example-text-input" class="col-3 col-form-label">
                    {!! __(\''.ucwords($fieldName).'\') !!}
                    <span class="text-danger">*</span>
                </label>
                <div class="col-7">
                    '.$input.'
                </div>
              </div>';
        return $field;
    }

    public function file($fieldName, $variable){
        $requiredInput = '{!! Form::file(\''.$fieldName.'\', [\'id\'=>\'image\', \'accept\'=>\'image/*\', \'required\'=>\'required\']) !!}';
        $unRequiredInput = '{!! Form::file(\''.$fieldName.'\', [\'id\'=>\'image\', \'accept\'=>\'image/*\']) !!}';
        $field ='<div class="form-group m-form__group row">
                <label for="example-text-input" class="col-3 col-form-label">
                    {!! __(\''.ucwords($fieldName).'\') !!}
                    <span class="text-danger">*</span>
                </label>
                <div class="col-7">
                @if($'.$variable.'->image > 0)
                    '.$unRequiredInput.'
                @else
                    '.$requiredInput.'
                @endif
                </div>
              </div>
              @if(!empty($'.$variable.'->image))
                <div class="form-group m-form__group row">
                    <label for="example-text-input" class="col-3 col-form-label">
                    </label>
                    <div class="col-7">
                        <img src="{!! imageUrl(url($'.$variable.'->image), 100, 80, 100, 1) !!}">
                    </div>
                </div>
              @endif
              ';
        return $field;
    }

    public function select($fieldName, $fieldType, $isRequired){
        $selectOptions[''] = 'Select';
        foreach ($fieldType as $key=>$option){
            if ($key > 0){
                $selectOptions[strtolower($option)] = ucwords($option);
            }
        }
        $options = json_encode($selectOptions);
        $options = str_replace(':', '=>', $options);
        $options = str_replace('{', '[', $options);
        $options = str_replace('}', ']', $options);
        $options = str_replace('"', "'", $options);
        $input = '{!! Form::select(\''.$fieldName.'\', '.$options.', old(\''.$fieldName.'\'), [\'class\' => \'form-control\', '.$isRequired.']) !!}';
        $field ='<div class="form-group m-form__group row">
                <label for="example-text-input" class="col-3 col-form-label">
                    {!! __(\''.ucwords($fieldName).'\') !!}
                    <span class="text-danger">*</span>
                </label>
                <div class="col-7">
                    '.$input.'
                </div>
              </div>';
        return $field;
    }

    public function radio($fieldName, $fieldType, $isRequired){
        $inputs = [];
        foreach ($fieldType as $key=>$option){
            if ($key > 0){
                $inputs[$key] = '{!! Form::radio(\''.strtolower($fieldName).'\', \''.$option.'\', ((old(\''.strtolower($fieldName).'\')==\''.$option.'\')? true : false), ['.$isRequired.']) !!}<br/>';
            }
        }
        $inputs = implode(' ', $inputs);

        $field ='<div class="form-group m-form__group row">
                <label for="example-text-input" class="col-3 col-form-label">
                    {!! __(\''.ucwords($fieldName).'\') !!}
                    <span class="text-danger">*</span>
                </label>
                <div class="col-7">
                '.$inputs.'
                </div>
              </div>';
        return $field;
    }

    public function checkbox($fieldName, $fieldType, $isRequired){
        $inputs = [];
        foreach ($fieldType as $key=>$option){
            if ($key > 0){
                $inputs[$key] = '{!! Form::checkbox(\''.strtolower($fieldName).'\', \''.$option.'\', ((old(\''.strtolower($fieldName).'\')==\''.$option.'\')? true : false), ['.$isRequired.']) !!}<br/>';
            }
        }
        $inputs = implode(' ', $inputs);

        $field ='<div class="form-group m-form__group row">
                <label for="example-text-input" class="col-3 col-form-label">
                    {!! __(\''.ucwords($fieldName).'\') !!}
                    <span class="text-danger">*</span>
                </label>
                <div class="col-7">
                '.$inputs.'
                </div>
              </div>';
        return $field;
    }

    private function editTabs($isLanguage, $view){
        $edit = [];
        $tabs = [];
        $forms = [];
        if ($isLanguage){
            $locales =  Language::all();
            foreach ($locales as $key=>$language){
                if ($language->short_code == 'en'){
                    $tabs[$language->title] = '<li class="nav-item m-tabs__item">
                                <a class="nav-link m-tabs__link active" data-toggle="tab" href="#tab_'.$language->short_code.'" role="tab" id="test'.$key.'">
                                    <i class="flaticon-share m--hide"></i>
                                    '.$language->title.'
                                </a>
                            </li>';
                    $forms[$language->title] = '<div class="tab-pane active" id="tab_'.$language->short_code.'">
                        @include(\'admin.'.$view.'.form\', [\'languageId\' => $locales[\''.$language->short_code.'\']])
                    </div>';
                }else{
                    $tabs[$language->title] = '<li class="nav-item m-tabs__item">
                                    <a class="nav-link m-tabs__link " data-toggle="tab" href="#tab_'.$language->short_code.'" role="tab" id="test'.$key.'" >
                                        <i class="flaticon-share m--hide"></i>
                                        '.$language->title.'
                                    </a>
                                </li>';
                    $forms[$language->title] = '<div class="tab-pane " id="tab_'.$language->short_code.'">
                            @include(\'admin.'.$view.'.form\', [\'languageId\' => $locales[\''.$language->short_code.'\']])
                        </div>';
                }
            }
        }else{
            $tabs['English'] = '<li class="nav-item m-tabs__item">
                                <a class="nav-link m-tabs__link active" data-toggle="tab" href="#tab_en" role="tab" id="test1">
                                    <i class="flaticon-share m--hide"></i>
                                    '.str_singular(ucwords($view)).'
                                </a>
                            </li>';
            $forms['English'] = '<div class="tab-pane active" id="tab_en">
                        @include(\'admin.'.$view.'.form\', [\'languageId\' => $locales[\'en\']])
                    </div>';
        }
        $edit['tabs'] = $tabs;
        $edit['forms'] = $forms;
        return $edit;

    }
}
