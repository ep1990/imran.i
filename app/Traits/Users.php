<?php

namespace App\Traits;

//use Twilio;
use Aloha\Twilio\Twilio;
use App\Http\Libraries\Uploader;
use App\Http\Requests\SaveUser;
use App\Http\Requests\UserLogin;
use App\Jobs\SendMail;
use App\Models\User;
use App\Models\Product;
use Illuminate\Foundation\Auth\SendsPasswordResetEmails;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Password;
use phpDocumentor\Reflection\Types\Null_;
use Twilio\Jwt\JWT;
use Tymon\JWTAuth\Facades\JWTAuth;

trait Users {

    use EMails,
        SendsPasswordResetEmails,
        Upload,
        WebServicesDoc,
        PluckPivot;

    public function registerUser($request, $fromWeb = false) {
        $data = $request->only([
            'first_name','last_name','chef_name','store_name', 'email', 'user_phone', 'gender', 'verify_with', 'address','location','type'
        ]);
        if ($request->has('google_id')) {
            $data['google_id'] = $request->get('google_id');
        }if ($request->has('facebook_id')) {
            $data['facebook_id'] = $request->get('facebook_id');
        }if($request->has('access_token')) {
            $instagramUserData = $this->getData("https://api.instagram.com/v1/users/self/?access_token=" . $request->get('access_token'));
            if($instagramUserData->meta->code == 400)
            {
                return responseBuilder()->error('Access token is not valid');
            }
            $data['instagram_id'] = $instagramUserData->data->id;
            
        }
        $data['latitude'] = $data['location'][1];
        $data['longitude'] = $data['location'][0];
        $existingUser = User::where(['email' => $data['email'], 'deleted_at' => null])->first();
        if (isset($existingUser)) {
            return responseBuilder()->error(__('Email already exist. Try with other Email Address'));
        } else {
            $data['password'] = bcrypt($request->get('password'));
            $data['verification_code'] = rand(1000, 9999);
            $user = User::create($data);
            if ($data['verify_with'] == 'email') {
                $data['email_verified'] = 0;
                $data['receiver_email'] = $data['email'];
                $data['receiver_name'] = $request->has('first_name')? $data['first_name'].' '.$data['last_name']:$request->chef_name;
                $data['sender_name'] = config('settings.company_name');
                $data['sender_email'] = config('settings.email');
                $data['view'] = 'emails.email_verification';
                $data['subject'] = 'Email verification code';
                SendMail::dispatch($data);
            } else {
                $this->sendVerificationCodeSMS($user);
            }
            $user = User::where(['id' => $user->id])->first();
            $customClaims['fcm_token'] = $request->get('fcm_token', '');
            $customClaims['user_id'] = $user->id;
            $token = JWTAuth::fromUser($user, $customClaims);
            $userData = $this->userData($user, $token, $fromWeb);
            return responseBuilder()->setAuthorization($token)->success(__('Registration successful'), $userData);
        }
    }


    public function registerSupplier($request, $fromWeb = false) {
        $data = $request->only([
            'email', 'phone_number', 'verify_with', 'address', 'location','fcm_token'
        ]);
        if ($request->has('google_id')) {
            $data['google_id'] = $request->get('google_id');
        }if ($request->has('facebook_id')) {
            $data['facebook_id'] = $request->get('facebook_id');
        }if($request->has('access_token')) {
            $instagramUserData = $this->getData("https://api.instagram.com/v1/users/self/?access_token=" . $request->get('access_token'));
            if($instagramUserData->meta->code == 400)
            {
                return responseBuilder()->error('Access token is not valid');
            }
            $data['instagram_id'] = $instagramUserData->data->id;
        }
        $data['description->en'] = '';
        $data['description->ar'] = ''; 
        $data['supplier_name->en'] = '';
        $data['supplier_name->ar'] = '';
        if($request->has('description'))
        {
            if(array_key_exists('en', $request->description))
            {
                $data['description->en'] = $request->description['en'];
            }
            if(array_key_exists('ar', $request->description))
            {
                $data['description->ar'] = $request->description['ar'];
            }
        }
        if(array_key_exists('en', $request->supplier_name))
        {
            $data['supplier_name->en'] = $request->supplier_name['en'];
        }
        if(array_key_exists('ar', $request->supplier_name))
        {
            $data['supplier_name->ar'] = $request->supplier_name['ar'];
        }
        
        $data['latitude'] = $data['location'][1];
        $data['longitude'] = $data['location'][0];
        $data['is_supplier'] = 1;
        $existingUser = User::where(['email' => $data['email'], 'deleted_at' => null])->first();
        if (isset($existingUser)) {
            return responseBuilder()->error(__('Email already exist. Try with other Email Address'));
        } else {
            if ($request->file('trade_license')){
                $uploader = new Uploader();
                $uploader->setInputName('trade_license');
                if ($uploader->isValidFile()) {
                    $uploader->upload('license', $uploader->fileName);
                    if ($uploader->isUploaded()) {
                        $data['trade_license'] = $uploader->getUploadedPath();
                    }
                }
                if (!$uploader->isUploaded()) {
                    return responseBuilder()->error(__('Error in file uploading'));
                }
            }
            $data['password'] = bcrypt($request->get('password'));
            $data['verification_code'] = rand(1000, 9999);
            $user = User::create($data);
            if ($data['verify_with'] == 'email') {
                $data['is_verified'] = 0;
                $data['receiver_email'] = $data['email'];
                $data['receiver_name'] = $data['supplier_name->en'];
                $data['sender_name'] = config('settings.company_name');
                $data['sender_email'] = config('settings.email');
                $data['view'] = 'emails.email_verification';
                $data['subject'] = 'Email verification code';
                SendMail::dispatch($data);
                // $this->sendMail($data, 'emails.email_verification', 'Email verification code', $data['receiver_email'], $data['sender_email']);
            } else {
                $this->sendVerificationCodeSMS($user);
            }
            $select = ['id','supplier_name', 'is_supplier', 'address','email','phone_number','trade_license','is_verified','latitude','longitude','average_rating','description','fcm_token','is_notification'];
            $user = User::where(['id' => $user->id])->select($select)->first();
            $customClaims['fcm_token'] = $request->get('fcm_token', '');
            $customClaims['user_id'] = $user->id;
            $token = JWTAuth::fromUser($user, $customClaims);
            $userData = $this->userData($user, $token, $fromWeb);
            return responseBuilder()->setAuthorization($token)->success(__('Registration successful'), $userData);
        }
    }


    public function sendVerificationCodeSMS($user) {
        $code = rand(1000, 9999);
        $twilio = new Twilio(env('TWILIO_SID'), env('TWILIO_TOKEN'), env('TWILIO_FROM'));
        $twilio->message($user->phone, 'Your Rent Car Activation Code is ' . $code);
        $user->update(['verification_code' => $code, 'email_verified' => 0]);
        return true;
    }

    public function emailVerification($request, $fromWeb = false) {
        $response = null;
        $this->validate(request(), ['verification_code' => 'required']);

        if ($fromWeb){
            $user = $this->user;
        }
       else{
           $user = \request('jwt.user', new \stdClass());
           $token = \request('jwt.token', new \stdClass());
       }
        if ($user->verification_code == $request->get('verification_code')) {
            $user->update(['verification_code' => '', 'is_verified' => 1]);
            $userData = $this->userData($user, $token, $fromWeb);
            return $userData;
//            return responseBuilder()->success(__('Email verified'), $userData);
        }
//        return responseBuilder()->error(__('Verification code does not match'));
        return false;
    }

    public function resendVerificationCodeTrait($fromWeb = false) {
       if ($fromWeb){
           $user = $this->user;

       }
       else{
           $user = \request('jwt.user', new \stdClass());
           $token = \request('jwt.token', new \stdClass());
       }
        if ($user->email_verified == 0) {
            if ($user->verify_with == 'phone') {
                $this->sendVerificationCodeSMS($user);
            } else {
                $data['verification_code'] = rand(1000, 9999);
                $data['receiver_email'] = $user->email;
                $data['receiver_name'] = $user->full_name;
                $data['sender_name'] = config('settings.company_name');
                $data['sender_email'] = config('settings.email');
                $user->update(['verification_code' => $data['verification_code']]);
                SendMail::dispatch($data);
//                $this->sendMail($data, 'emails.email_verification', 'Email verification code', $data['receiver_email'], $data['sender_email']);
            }
            $userData = $this->userData($user, $token->get(), $fromWeb);
//            return responseBuilder()->success(__('Verification code sent'), $userData);
            return $userData;
        }
        return false;
//        return responseBuilder()->success(__('Email already verified'));
    }

    private function userData($user, $token, $fromWeb = false) {
        $userData = $user->toArray();
        if ($fromWeb) {
            $this->setWebData($userData, $user->password, $token);
        }
        foreach ($userData as $key => $value) {
            if (is_null($value)) {
                if ((strpos($key, '_id') !== FALSE) && ($key != 'facebook_id' || $key != 'google_id')) {
                    $userData[$key] = '';
                } else {
                    $userData[$key] = '';
                }
            }
            if ($key =='verification_code')
            {
                $userData[$key] = '';
            }
            if ($key =='latitude' || $key =='longitude')
            {
                $userData[$key] = round($userData[$key], 2);
            }
        }

        if($userData['is_supplier'] == 0){
            unset($userData['supplier_name']);
        }
        if($userData['description'] == "")
        {
            $userData['description'] = new \stdClass();
            $userData['description']->en = "";
            $userData['description']->ar = "";
        }
        $userData['token'] = 'Bearer ' . $token;
//        dd($userData);
        return $userData;
    }

    private function setWebData($userData, $password, $token) {
        auth()->loginUsingId($userData['id']);
        $userData += ['password' => $password, 'authorization' => $token];
        session()->put('USER_DATA', $userData);
    }

    function getData($url) {
        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => $url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_TIMEOUT => 30000,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "GET",
            CURLOPT_HTTPHEADER => array(
                'Content-Type: application/json',
            ),
        ));
        $response = curl_exec($curl);
        $err = curl_error($curl);
        curl_close($curl);
        if ($err) {
            return json_decode($err);
        } else {
            return json_decode($response);
        }
    }

    public function socialLogin($request) {
        $comparevalue = $request->get("google_id");
        $instagram = false;
        $socialLoginColumn = 'google_id';
        if (!empty($request->get('access_token'))) {
            // $instagramUserData = $this->getData("https://api.instagram.com/v1/users/self/?access_token=" . $request->get('access_token'));
            $comparevalue =  $request->get('access_token');
            $comparevalue =  explode('.',$comparevalue);
            $comparevalue = $comparevalue[0];
            $socialLoginColumn = 'instagram_id';
            $instagram = true;
        }
        if ($request->get('facebook_id')) {
            $socialLoginColumn = 'facebook_id';
            $comparevalue = $request->get("facebook_id");
        }if ($request->get('instagram_id')) {
            $socialLoginColumn = 'instagram_id';
        }
        if (empty($request->get("google_id")) && empty($request->get("facebook_id"))&& !$instagram)
        {
            return responseBuilder()->error(__('Could not find your account.'));
        }
        if ($comparevalue != 'undefined')
        {
            $userExists = User::where([$socialLoginColumn => $comparevalue])->first();
            if (!empty($userExists))
            {
                $user = $userExists;
                $customClaims['user_id'] = $user->id;
                $userToken = JWTAuth::fromUser($user, $customClaims);
                $userData = $this->userData($user, $userToken, false);
                return responseBuilder()->setAuthorization($userToken)->success(__('Social login user data.'), $userData);
            }else if($request->get("email") &&  $request->get("email") != 'undefined'){
                $userExists = User::where(['email' => $request->get("email")])->first();
                if (!empty($userExists))
                {
                    $user = $userExists;
                    $user->update([$socialLoginColumn => $comparevalue]);
                    $customClaims['user_id'] = $user->id;
                    $userToken = JWTAuth::fromUser($user, $customClaims);
                    $userData = $this->userData($user, $userToken, false);
                    return responseBuilder()->setAuthorization($userToken)->success(__('Social login user data.'), $userData);
                }

            }
        }
        return responseBuilder()->error(__('Could not find your account.'));
    }

    public function loginUser($request, $fromWeb = false) {
        try {
            $token = NULL;
            $user = NULL;
            $instagramUserData = NULL;
            $customClaims = ['fcm_token' => $request->get('fcm_token', '')];
            $requestData = $request->only(['email', 'password', 'google_id', 'facebook_id']);
            if (!empty($requestData['email']) && !empty($requestData['password'])) {
                // login with email & password
                $token = JWTAuth::attempt(['email' => $requestData['email'], 'password' => $requestData['password']]);
                if ($token) {
                  $user =   JWTAuth::setToken($token)->toUser();
                    $customClaims['user_id'] = $user->id;
                    $userToken = JWTAuth::fromUser($user, $customClaims);
//                    $user->load(['languages' , 'city.languages' , 'billingAddress' , 'shippingAddress']);
//                    $user = $this->setCityLanguages($user);
                    $userData = $this->userData($user, $userToken, $fromWeb);
                    return responseBuilder()->setAuthorization($userToken)->success(__('Login successful'), $userData);
                }
                return responseBuilder()->error(__('Credentials does not match our records'));
            }
            else
            {
                // login with google / facebook
                $socialLoginColumn = 'google_id';
                if (!empty($requestData['facebook_id'])) {
                    $socialLoginColumn = 'facebook_id';
                }if (!empty($requestData['instagram_id'])) {
                    $socialLoginColumn = 'instagram_id';
                }
                // check if user exists
                $userExists = User::where([$socialLoginColumn => $requestData[$socialLoginColumn]])->first();
                if (count($userExists) > 0) {
                    $user = $userExists;
                    $customClaims['user_id'] = $user->id;
                    if ($user->account_type == User::$ACCOUNT_TYPE_COMPANY) {
                        $customClaims['company_id'] = $user->companies->id;
                    }
                    $token = JWTAuth::fromUser($user, $customClaims);
                }
                else {
                    // try to find with email
                    $userExists = User::where(['email' => $requestData['email']])->first();
                    if (count($userExists) > 0) {
                        $userExists->$socialLoginColumn = $requestData[$socialLoginColumn];
                        $userExists->save();
                        $user = $userExists;
                        $customClaims['user_id'] = $user->id;
                        $token = JWTAuth::fromUser($user, $customClaims);
                    } else {
                        // create user
                        if (count($instagramUserData) > 0)
                        {
                            $userData['instagram_id'] = $instagramUserData->data->id;
                            $userData['full_name'] = $instagramUserData->data->full_name;
                            $userData['about'] = $instagramUserData->data->bio;
                        } else {
                            $userData = $request->only(['full_name', 'email', $socialLoginColumn]);
                        }
                        $userData['email_verified'] = 1;
                        $user = User::create($userData);
                        $customClaims['user_id'] = $user->id;
                        $token = JWTAuth::fromUser($user, $customClaims);
                    }
                }
                if ($token) {
                    $user->load(['languages' , 'city.languages']);
                    $user = $this->setCityLanguages($user);
                    $userData = $this->userData($user, $token, $fromWeb);
                    return responseBuilder()->setAuthorization($token)->success(__('Login successful'), $userData);
                }
                return responseBuilder()->error(__('Credentials does not match our records'));
            }
        } finally {
            if ($token) {
                
            }
        }
    }

    public function updateUserProfile($request, $fromWeb = false) {
        $user = \request('jwt.user', new \stdClass());
        $token = \request('jwt.token', new \stdClass());
        if($user->is_kiosk != 0)
        {
            return responseBuilder()->error(__('unauthorized'));
        }
        $data = $request->only(['first_name','last_name', 'phone_number', 'gender', 'address', 'location']);
        $data['latitude'] = $data['location'][1];
        $data['longitude'] = $data['location'][0];
        if($request->file('image'))
        {
            $imageResponse = $this->handleImage($request, $data, 'users','image');
            if ($imageResponse['status'] == -1) {
                return responseBuilder()->error($imageResponse['message']);
            }
            $data['image'] = url($imageResponse['path']);
        }
        $user->update($data);
        $select = ['id','first_name','last_name','email','address','gender','phone_number','image','is_verified','is_active','is_status','fcm_token','is_notification', 'is_supplier', 'description'];
        $user = User::select($select)->findOrFail($user->id);
        $userData = $this->userData($user, $token->get(), $fromWeb);
        return responseBuilder()->success(__('Profile saved'), $userData);
    }

    public function userProfileData($fromWeb = false) {
        if ($fromWeb){
            $user = $this->user;
            $token = JWTAuth::fromUser($user);
        }
        else
        {
            $user = JWTAuth::toUser(JWTAuth::getToken());
            $token = JWTAuth::fromUser($user);
        }
        $user->user_image = url($user->user_image);
        $userData = $this->userData($user, $token);
        return $userData;
    }

    public function supplierProfileData() {
        $userData = JWTAuth::toUser(JWTAuth::getToken());
        $user = User::select('id','supplier_name','address','image','email','phone_number',\DB::raw('concat("' . url('/') . '/",trade_license)as trade_license'),'is_verified','latitude','longitude','average_rating','description','fcm_token','is_notification','is_supplier')
        ->where(['id' => $userData->id, 'is_supplier' => 1])
        ->first();
        if(!$user){
            return responseBuilder()->error("Something went wrong");
        }
        $token = JWTAuth::fromUser($user);
        $userData = $this->userData($user, $token);
        return responseBuilder()->success(__('Supplier profile data'), $userData);
    }

    public function supplierProfileUpdateTrait($request, $fromWeb = false) {
        $user = \request('jwt.user', new \stdClass());
        if($user->is_supplier != 1)
        {
            return responseBuilder()->error(__('unauthorized'));
        }
        $token = \request('jwt.token', new \stdClass());
        $data = $request->only([
            'phone_number', 'address', 'location'
        ]);
        if(array_key_exists('en', $request->description)){
            $data['description->en'] = $request->description['en'];
        }
        if(array_key_exists('ar', $request->description)){
            $data['description->ar'] = $request->description['ar'];
        }
        if(array_key_exists('en', $request->supplier_name)){
            $data['supplier_name->en'] = $request->supplier_name['en'];
        }
        if(array_key_exists('ar', $request->supplier_name)){
            $data['supplier_name->ar'] = $request->supplier_name['ar'];
        }
        $data['latitude'] = $data['location'][1];
        $data['longitude'] = $data['location'][0];
        if ($request->file('trade_license')){
            $uploader = new Uploader();
            $uploader->setInputName('trade_license');
            if ($uploader->isValidFile()) {
                $uploader->upload('license', $uploader->fileName);
                if ($uploader->isUploaded()) {
                    $data['trade_license'] = $uploader->getUploadedPath();
                }
            }
            if (!$uploader->isUploaded()) {
                return responseBuilder()->error(__('Error in file uploading'));
            }
        }
        if ($request->file('image')){
            $uploader = new Uploader();
            $uploader->setInputName('image');
            if ($uploader->isValidFile()) {
                $uploader->upload('suppliers', $uploader->fileName);
                if ($uploader->isUploaded()) {
                    $data['image'] = $uploader->getUploadedPath();
                }
            }
            if (!$uploader->isUploaded()) {
                return responseBuilder()->error(__('Error in file uploading'));
            }
        }
        $user->update($data);
        // CHANGE PRODUTS ADDRESS AND LAT LONG //
        Product::where('user_id', $user->id)->update(['latitude' => $user->latitude, 'longitude' => $user->longitude, 'address' => $user->address]);
        $user = User::where(['id' => $user->id])
        ->select('id','supplier_name','address','email','phone_number',\DB::raw('concat("' . url('/') . '/",trade_license)as trade_license'),'image','is_verified','latitude','longitude','average_rating','description','fcm_token','is_notification','is_supplier')
        ->first();
        $userData = $this->userData($user, $token->get(), $fromWeb);
        return responseBuilder()->success(__('Supplier profile saved'), $userData);
    }

    public function changeUserPassword($request, $fromWeb = false) {
        $user = \request('jwt.user', new \stdClass());
        $token = \request('jwt.token', new \stdClass());
        if (\Hash::check($request->get('current_password'), $user->password)) {
            $user->update(['password' => bcrypt($request->get('password'))]);
            $user->user_image = url($user->user_image);
            $userData = $this->userData($user, $token->get(), $fromWeb);
            return responseBuilder()->success(__('Password updated'), $userData);
        }
        return responseBuilder()->error(__('Current password does not match'));
    }

    public function sendResetLinkEmail($request, $fromWeb = false) {
        $this->validate(request(), [
            'email' => 'required | exists:users,email'
        ]);
        $code = rand(1000, 9999);
            $user = User::where(['email' => $request->get('email')])->first();
            if ($user !== null) {
                $data['receiver_name'] = $user->first_name.' '.$user->last_name;
                $data['receiver_email'] = $user->email;
                $data['sender_name'] = 'My tech';
                $data['sender_email'] = config('settings.email');
                $data['verification_code'] = $code;
                $data['view'] = 'emails.forgot_password';
                $data['subject'] = 'Email verification code';
                $user->update(['verification_code' => $code]);
                SendMail::dispatch($data);
//                $this->sendMail($data, 'emails.forgot_password', 'Email verification code', $data['receiver_name'], $data['sender_email']);
                return true;
            } else {
                return false;
            }
// else {
//            $user = User::where(['phone' => $request->get('email_or_phone')])->first();
//            if (count($user) > 0) {
//                $this->sendVerificationCodeSMS($user);
//                return responseBuilder()->success(__('We have sent verification code for reset your password.'));
//            } else {
//                return responseBuilder()->error(__('Record does`nt match.'));
//            }
//        }
    }

    public function resetPasswordTrait($request, $fromWeb = false) {
        // dd($request->all());
        $user = null;
        $user = User::where([
                        'verification_code' => $request->get('verification_code'),
                        'email' => $request->get('email')])->first();
//        else {
//            $user = User::where([
//                        'verification_code' => $request->get('verification_code'),
//                        'phone' => $request->get('email_or_phone')])->first();
//        }
        if ($user !== null) {
            $user->update(['password' => bcrypt($request->get('password'))]);
            return true;
        } else {
            return false;
        }
    }

    public function logout($token) {
        $user = \request('jwt.user', new \stdClass());
        $token = \request('jwt.token', new \stdClass());
        $fcmToken = JWTAuth::getPayload($token)->get('fcm_token');
        Device::where(['user_id' => $user->id, 'fcm_token' => $fcmToken])->delete();
        JWTAuth::invalidate($token->get());
        return responseBuilder()->success(__('Logout successful'));
    }

    public function setCityLanguages($user)
    {
        if (sizeof($user->languages) > 0)
        {
            $user->translation = $this->translateRelation($user);
        }
        else
        {
            $user->translation = new \stdClass();
        }
        $user->city->translation = $this->translateRelation($user['city']);
        unset($user->city->languages);
        unset($user->languages);
        return $user;
    }

    public function editUserPaymentProfile( $fromWeb = false){
        if ($fromWeb){
            $user = $this->user;

        }
        else{
            $user = \request('jwt.user', new \stdClass());
            $token = \request('jwt.token', new \stdClass());
        }

        return $user->only('client_id','secret_id');

    }

    public function saveUserPaymentProfile( $request, $fromWeb = false){
        if ($fromWeb){
            $user = $this->user;

        }
        else{
            $user = \request('jwt.user', new \stdClass());
            $token = \request('jwt.token', new \stdClass());
        }
        $data = $request->only('client_id','secret_id');
        $user->update($data);
        return  $user;

    }


}
