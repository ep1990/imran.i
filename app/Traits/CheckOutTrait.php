<?php


namespace App\Traits;


use App\Models\Address;
use App\Models\Cart;

trait CheckOutTrait
{
    public function checkoutIndex( $fromWeb= false){
        if ($fromWeb){
            $user = $this->user;
        }
        else{
            $user = \request('jwt.user', new \stdClass());
            $token = \request('jwt.token', new \stdClass());
        }
        $cart =  Cart::where('user_id',$user->id)->get();
        $subTotal = 0;
        foreach ($cart as $key => $value) {
            $subTotal += $value->total_price;
        }
        $billing = Address::where(['user_id'=> $user->id,'type'=>'billing'])->first();
        $shipping = Address::where(['user_id'=> $user->id,'type'=>'shiping'])->first();
        if (empty($shipping)){
            $shipping=new Address();
            $shipping->id=0;
        }
        if($billing)
        {
            $address['billing'] = $billing;
            $address['shipping'] = $shipping;
            $address['subTotal'] = $subTotal;

            return $address;
        }
        else{
            $billing=$user;
            $shipping=new Address();
            $billing->id=0;
            $shipping->id=0;
            $address['billing'] = $billing;
            $address['shipping'] = $shipping;
            $address['subTotal'] = $subTotal;
            return $address;
        }
    }

    public function updateCheckOutAddress($request,$fromWeb = false)
    {
        if ($fromWeb) {
            $user = $this->user;
        } else {
            $user = \request('jwt.user', new \stdClass());
            $token = \request('jwt.token', new \stdClass());
        }
        $data = $request->only('first_name', 'last_name', 'email', 'user_phone', 'address', 'street_address', 'post_code');
        $data['user_id'] = $user->id;
        $data['type'] = 'billing';
        $billing = Address::updateOrCreate(['user_id' => $data['user_id'], 'type' => 'billing'], $data);
        $shipping = new Address();
        $shipping->id = 0;
        if ($request->shipping_to == '1') {
            $data['first_name'] = $request->shipping_first_name;
            $data['last_name'] = $request->shipping_last_name;
            $data['email'] = $request->shipping_email;
            $data['user_phone'] = $request->shipping_user_phone;
            $data['address'] = $request->shipping_address;
            $data['street_address'] = $request->shipping_street_address;
            $data['post_code'] = $request->shipping_post_code;
            $data['type'] = 'shiping';
            $shipping = Address::updateOrCreate(['user_id' => $data['user_id'], 'type' => 'shipping'], $data);

        }
        $cart =  Cart::where('user_id',$user->id)->get();
        $subTotal = 0;
        foreach ($cart as $key => $value) {
            $subTotal += $value->total_price;
        }
        $address['billing'] = $billing;
        $address['shipping'] = $shipping;
        $address['sub_total'] = $subTotal;
        return $address;


    }



}