<?php


namespace App\Traits;


use App\Models\Address;
use App\Models\Cart;
use App\Models\Order;
use App\Models\OrderDetail;
use App\Models\RiderOrder;
use App\Models\Notification;
use App\Models\{User,Coupon};
use Carbon\Carbon;
use PayPal\Api\Payment;
use App\Traits\CartTrait;
use App\Traits\MyTechnologyPayPal;

trait OrderTrait
{
    use  MyTechnologyPayPal, CartTrait;
    public function createOrderTrait($request,$fromWeb = false){
        if ($fromWeb){
            $user = $this->user;
        }
        else{
            $user = \request('jwt.user', new \stdClass());
        }
        $billing = Address::where(['user_id'=> $user->id,'type' => 'billing'])->first();
        if ($billing == null){
            return  'billing';
        }
        if ($request->shipping_to_diff != ''){
            $shipping = $billing;
            $shipping = Address::where(['user_id'=> auth()->user()->id,'type' => 'shipping'])->first();
            if ($shipping == null){
                return 'shipping';
            }
        }
        $cartData = $this->getCartData($user);
        // $total = $cartData['sub_total']->aed->amount + $cartData['shipping_total']->aed->amount + $cartData['vat']->aed->amount;

        if (count($cartData['data'])>0){

            if (!$fromWeb && $request->has('payment_method') && $request->payment_method == 'paypal'){
                $paymentResponse = $this->checkPaypal($user,$request->payment_id);
                if (!$paymentResponse){
                    return 'payment error';
                }
                if ($paymentResponse['transactions'][0]['amount']['total'] == round($cartData['grand_total']->usd->amount,2)){
                    $order =  $this->orderCreate($cartData,$request, $billing,$user,$paymentResponse);
                    return $order;
                }
                else{
                    return 'payment incorrect';
                }
                $order =  $this->orderCreate($cartData,$request, $billing,$user, $paymentResponse);
                return $order;
            }
            if ($request->has('payment_method')  && $request->payment_method == 'cash_on_delivery'){
                $order =  $this->orderCreate($cartData,$request, $billing,$user);
                return $order;
            }
        }
        else{
            return 'noData';
        }

    }

    public function orderCreate($cartData,$request, $billing,$user,$paymentData = false){
        if ($request->shipping_to_diff == ''){
            $shipping = $billing;
        }
        else{
            $shipping = Address::where(['user_id'=> $user->id,'type' => 'shipping'])->firstOrFail();
        }

        $coupon = '';
        if($request->has('coupon') && $request->coupon != '')
        {
            $coupon = Coupon::where('code', $request->coupon)->first();
        }

    //    $total =  $cartData['sub_total']->aed->amount+ $cartData['shipping_total']->aed->amount + $cartData['vat']->aed->amount;
       $total =  $cartData['sub_total']->aed->amount;
        $data['billing'] = json_encode($billing);
        $data['shipping'] = json_encode($shipping);
        $data['aedPrice']=
        $data['aedRate']=getConversionRate();
        $today = Carbon::now();
        $orderNumber = $today->yearIso . $today->month . $today->day . '-' . $today->timestamp;
        $order = Order::create([
            'user_id' => $user->id,
            'first_name' => $user->first_name,
            'last_name' => $user->last_name,
            'order_note' => $request->order_note,
            'billing_address' =>$data['billing'],
            'shipping_address' => $data['shipping'],
            'order_number' => $orderNumber,
            'payment_status' => (isset($paymentData['payer']['status']) == 'approved') ? 'confirmed' : '',
            'order_status' => 'pending',
            'image' => $cartData['data'][0]->product->image,
            'payment_method' => $request->payment_method,
            'total_amount' => $total,
            'aed_price'=>$total,
            'vat' => $cartData['vat']->aed->amount,
            'payment_id' => (isset($paymentData['id'])) ? $paymentData['id'] : '',
            'payer_status' => (isset($paymentData['payer']['status'])) ? $paymentData['payer']['status'] : '',
            'payer_email' => (isset($paymentData['payer']['payer_info']['email'])) ? $paymentData['payer']['payer_info']['email'] : '',
            'payer_id' => (isset($paymentData['payer']['payer_info']['payer_id'])) ? $paymentData['payer']['payer_info']['payer_id'] : '',
            'charges' => (isset($paymentData['transactions'][0]['amount']['total'])) ? $paymentData['transactions'][0]['amount']['total']: 0,
            'currency' => (isset($paymentData['transactions'][0]['amount']['currency'])) ? $paymentData['transactions'][0]['amount']['currency'] : 'AED',
            'transaction_details' => (isset($paymentData['transactions'][0]['amount']['details'])) ? json_encode($paymentData['transactions'][0]['amount']['details']) : '',
            'paypal_response' => json_encode($paymentData),
            'total_shipping' => $cartData['shipping_total']->aed->amount,
            'coupon_code' => ($coupon != '')?$coupon->code:'',
            'coupon_percent' => ($coupon != '')?$coupon->percent:'',
        ]);
        if($coupon){
            $coupon->update(['status' => 'used']);
        }
        User::where('id', $user->id)->update(['coupon_id' => NULL]);
        $this->createOrderDetail($order,$cartData, $user);
        $title = ['en' => 'Order Placed', 'ar' => 'تم الطلب'];
        $description = ['en' => 'Your order is placed', 'ar' => 'يتم وضع طلبك'];
        $title = ['en' => 'Order Placed', 'ar' => 'تم الطلب'];
        $description = ['en' => 'Your order is placed', 'ar' => 'يتم وضع طلبك'];
        $extras = ['order_id' => $order->id];
        createNotification($user, $user->id, 'order_placed', $title, $description, $extras);
        Cart::where('user_id', $user->id)->delete();
        return $order;
    }

    public function checkPaypal($user,$payment_id){
        $this->initPayPal();
        $paymentId = $payment_id;
        $payment = Payment::get($paymentId, $this->apiContext);
        if ($payment->state == 'approved'){
            return $payment->toArray();
        }
        return false;
    }

    public function createOrderDetail($order,$cartData, $user){
        $suppliers = [];
        foreach ($cartData['data'] as $cartKey => $cartValue){
            if(!in_array($cartValue->product->user_id, $suppliers))
            {
                $suppliers[] = $cartValue->product->user_id;
                $supplier = User::where('id', $cartValue->product->user_id)->first();
                $title = ['en' => 'New Order', 'ar' => 'طلب جديد'];
                $description = ['en' => 'Your order is placed', 'ar' => 'لقد تلقيت طلب جديد'];
                $extras = ['order_id' => $order->id];
                createNotification($supplier, $user->id, 'new_order', $title, $description, $extras);
            }
            OrderDetail::create([
                'order_id'=> $order->id,
                'user_id' => $cartValue->user_id,
                'supplier_id' => $cartValue->product->user_id,
                'product_id' => $cartValue->product_id,
                'product_price' => $cartValue['product_price']->aed->amount,
                'quantity' => $cartValue->quantity,
                'total_price' => $cartValue['total_price']->aed->amount,
                'image' => $cartValue->product->getoriginal('image')
            ]);
        }
        return true;
    }

    public function userOrdersList($status,$fromWeb = false){
        if ($fromWeb){
            $user = $this->user;
        }
        else{
            $user = \request('jwt.user', new \stdClass());
        }
        $pending = function ($pending){
            $pending->where('item_status','pending');
        };
        $shipping = function ($pending){
            $pending->where('item_status','in-progress');
        };
        $canceled = function ($canceled){
            $canceled->where('item_status','canceled');
        };
        $complete= function ($complete){
            $complete->where('item_status','complete');
        };
        if ($status == 'all'){
         $where =    ['user_id' => $user->id];
        }
        else{
            $where =    ['user_id' => $user->id,'order_status' => $status];
        }

        $orders = Order::select('id', 'order_status','aed_price','coupon_code','coupon_percent','order_number','image','vat','total_amount','total_shipping','created_at')->whereHas('orderDetails')->withcount(['orderDetails','orderDetails as pending' => $pending,'orderDetails as shipping' => $shipping,'orderDetails as canceled' => $canceled,'orderDetails as completed' => $complete, ])
            ->where($where)
            ->orderBy('updated_at','DESC')
            ->paginate(5);
        $orders =  $this->setPrice($orders);

        return $orders;

    }

    public function supplierOrdersList($status,$fromWeb = false){
        if ($fromWeb){
            $user = $this->user;
        }
        else{
            $user = \request('jwt.user', new \stdClass());
        }
        $pending = function ($pending) use($user){
            $pending->where(['item_status' => 'pending', 'supplier_id' => $user->id]);
        };
        $shipping = function ($pending) use($user){
            $pending->where(['item_status' => 'in-progress', 'supplier_id' => $user->id]);
        };
        $canceled = function ($canceled) use($user){
            $canceled->where(['item_status' => 'canceled', 'supplier_id' => $user->id]);
        };
        $complete= function ($complete) use($user){
            $complete->where(['item_status' => 'complete', 'supplier_id' => $user->id]);
        };

        $checkOderDetail = function($order) use($user ,$status){
            if ($status !=='all'){
                $where = ['supplier_id'=>$user->id,'item_status' => $status];
            }else{
                $where = ['supplier_id'=>$user->id];
            }
            $order->where($where);
        };
        $selectOrderDetail = function ($order){
            $order->select('id','order_id','total_price');
        };
        $orders = Order::select('id','aed_price', 'order_number','image','vat','total_amount','total_shipping','coupon_code','coupon_percent','created_at')->whereHas('orderDetails',$checkOderDetail)->withcount(['orderDetails as pending' => $pending,'orderDetails as shipping' => $shipping,'orderDetails as canceled' => $canceled,'orderDetails as completed' => $complete ])
            ->with(['orderDetails' => function($q) use($user){ return $q->where('supplier_id', $user->id); }])
            ->orderBy('updated_at','DESC')
            ->paginate(5);
        return $this->setPriceSupplier($orders);
    }


    public function setPriceSupplier($orders)
    {
        $total_price = 0;
        $vat = config('settings.value_added_tax');
        foreach($orders as $key =>$order){
            $total_amount = 0;
            $coupon_percent = 0;
            $count = 0;
            $status = '';
            if($order->coupon_percent > 0){
                $coupon_percent = $order->coupon_percent;
            }
            foreach($order->orderDetails as $key2 => $value){
                $total_amount += $value->total_price;
                $value->total_price = getPriceObject($value->total_price);
                $count++;
                $status = $value->item_status;

            }
            // unset($order->total_amount, $order->vat, $order->aed_price);
            $coupon_amount = $total_amount/100*$coupon_percent;
            $sub_amount = $total_amount;
            $total_amount = $total_amount-$coupon_amount;
            $vat_amount = $total_amount/100 *$vat;
            $total_amount = $total_amount+$vat_amount;
            $order->total_amount = getPriceObject($total_amount);
            $order->total_products = $count;
            $order->order_status = $status;
            $order->vat = getPriceObject($vat_amount);
        }
        return $orders;
    }

    public function setPrice($orders){
        $rate = getConversionRate();
        foreach($orders as $key => $order){
            $coupon_percent = 0;
            if($order->coupon_percent > 0){
                $coupon_percent = $order->coupon_percent;
            }
            $coupon_amount = $order->total_amount/100*$coupon_percent;
            $total_amount = $order->total_amount-$coupon_amount;
            $total_amount = $total_amount+$order->vat;
            $order->total_products = $order->order_details_count;
            $order->total_amount = getPriceObject($total_amount);
            $order->price = getPriceObject($order->aed_price);
            $order->vat = getPriceObject($order->vat);
            $order->total_shipping = getPriceObject($order->total_shipping);
            unset($order->order_details_count);
            unset($order->aed_price);
        }
        return $orders;
    }

    public  function userOrdersDetail($order_id,$fromWeb = false){
        if ($fromWeb){
            $user = $this->user;
        }
        else{
            $user = \request('jwt.user', new \stdClass());
        }
        $checkProduct = function($checkProduct){
            $checkProduct->whereHas('product');
        };

        $pending = function ($pending){
            $pending->where('item_status','pending');


        };
        $shipping = function ($pending){
            $pending->where('item_status','shipping');
        };
        $canceled = function ($canceled){
            $canceled->where('item_status','canceled');
        };
        $complete= function ($complete){
            $complete->where('item_status','complete');
        };
        
        $selectOrderDetails = function ($order){
          $order->whereHas('product')->with(['product' =>function($product){
              $product->select('id','price','user_id','title','image')
                  ->whereHas('supplier')->with(['supplier' => function($supplier){
                      $supplier->select('id','supplier_name');
                  }]);
          }])  ;
        };
        $order = Order::select('id','aed_price', 'order_status','coupon_code','coupon_percent','vat','total_amount','total_shipping','order_note','billing_address','shipping_address','order_number','payment_method')
            ->whereHas('orderDetails',$checkProduct)
            ->with(['orderDetails' => $selectOrderDetails])
            ->withCount(['orderDetails','orderDetails as pending' => $pending,'orderDetails as shipping' => $shipping,'orderDetails as canceled' => $canceled,'orderDetails as completed' => $complete])
            ->where(['id'=>$order_id,'user_id' => $user->id])->firstOrFail();
        $rate =getConversionRate();
        $coupon_percent = 0;
        if($order->coupon_percent > 0){
            $coupon_percent = $order->coupon_percent;
        }
        $coupon_amount = $order->total_amount/100*$coupon_percent;
        $total_amount = $order->total_amount-$coupon_amount;
        $total_amount = $total_amount-$order->vat;
        $sub_total = $order->total_amount;
        $order->billing_address = json_decode( $order->billing_address);
        $order->shipping_address = json_decode( $order->shipping_address);
        $order->aed_price = getPriceObject( $order->aed_price);
        $order->vat = getPriceObject($order->vat) ;
        $order->total_amount = getPriceObject($total_amount);
        $order->sub_total = getPriceObject($sub_total);
        $order->coupon_amount = getPriceObject($coupon_amount);
        $order->total_shipping = getPriceObject($order->total_shipping);
        $order->total_products = $order->order_details_count;
        unset($order->order_details_count);
        return $this->setOrderDetailPrice($order);
    }
    public function setOrderDetailPrice($order){
        $rate =getConversionRate();
        foreach ($order->orderDetails as $key => $orderDetail){
            $orderDetail->product_price = getPriceObject($orderDetail->product_price);
            $orderDetail->total_price = getPriceObject( $orderDetail->total_price);
            $orderDetail->product['price'] = getPriceObject($orderDetail->product['price']) ;
        }
        return $order;
    }

    public function supplierOrdersDetailTrait($order_id, $fromWeb = false){

        if ($fromWeb){
            $user = $this->user;
        }
        else{
            $user = \request('jwt.user', new \stdClass());
        }
        $checkProduct = function($checkProduct) use($order_id,$user){
            $checkProduct->whereHas('product')->where(['order_id' => $order_id,'supplier_id' => $user->id]);
        };
        $pending = function ($pending) use($user){
            $pending->where(['item_status' => 'pending', 'supplier_id' => $user->id]);
        };
        $shipping = function ($pending) use($user){
            $pending->where(['item_status' => 'shipping', 'supplier_id' => $user->id]);
        };
        $canceled = function ($canceled) use($user){
            $canceled->where(['item_status' => 'canceled', 'supplier_id' => $user->id]);
        };
        $complete= function ($complete) use($user){
            $complete->where(['item_status' => 'complete', 'supplier_id' => $user->id]);
        };
        $selectOrderDetails = function ($order) use($user, $order_id){
            $order->where(['order_id' => $order_id,'supplier_id' => $user->id])->whereHas('product')->with(['product' =>function($product){
                $product->select('id','price','user_id','title',\DB::raw('concat("' . url('/') . '/",image)as image'))
                    ->whereHas('supplier')->with(['supplier' => function($supplier){
                        $supplier->select('id','supplier_name');
                    }]);
            }])  ;
        };
        $order = Order::select('id', 'user_id','order_note','billing_address','shipping_address','order_number','payment_method','created_at','coupon_code', 'coupon_percent')
        ->whereHas('orderDetails',$checkProduct)->with(['orderDetails' => $selectOrderDetails, 'user:id,first_name,last_name,image,email,phone_number'])->withCount(['orderDetails as pending' => $pending,'orderDetails as shipping' => $shipping,'orderDetails as canceled' => $canceled,'orderDetails as completed' => $complete])
            ->firstOrFail();
        $order->total_products = $order->pending + $order->shipping + $order->canceled + $order->completed ;
        $total = 0;
        $totalShipping = 0;
        $coupon_percent = 0;
        if($order->coupon_percent > 0){
            $coupon_percent = $order->coupon_percent;
        }
        foreach ($order->orderDetails as $key => $orderDetail){
            $total += $orderDetail->total_price;
            $totalShipping  += $orderDetail->product['service_fee'] + $orderDetail->product['delivery_fee'];
            $orderDetail->product['service_fee'] = getPriceObject($orderDetail->product['service_fee']);
            $orderDetail->product['delivery_fee'] = getPriceObject($orderDetail->product['delivery_fee']);
            $orderDetail->product['price'] = getPriceObject($orderDetail->product['price']);
            $orderDetail->product_price = getPriceObject($orderDetail->product_price);
            $orderDetail->total_price = getPriceObject( $orderDetail->total_price);
        }
        $vat = config('settings.value_added_tax');
        $coupon_amount = $total/100*$coupon_percent;
        $sub_total = $total;
        $total = $total-$coupon_amount;
        $vat_amount = $total/100 *$vat;
        $total = $total+$vat_amount;
        $order->billing_address = json_decode( $order->billing_address);
        $order->shipping_address = json_decode( $order->shipping_address);
        $order->total_amount = getPriceObject($total);
        $order->total_shipping = getPriceObject($totalShipping);
        $order->sub_total = getPriceObject($sub_total);
        $order->coupon_amount = getPriceObject($coupon_amount);
        $order->total_shipping = getPriceObject($totalShipping);
        $order->vat = getPriceObject($vat_amount);
        $order->order_status = $order->orderDetails[0]->item_status;
        return $order;
    }

    public function supplierCancelOrderTrait($request, $fromWeb = false){
        if ($fromWeb){
            $user = $this->user;
        }
        else{
            $user = \request('jwt.user', new \stdClass());
        }
        if($user->is_supplier != 1){
            return 'unauthorized';
        }
        $order =  OrderDetail::where(['order_id'=>$request->order_id,'supplier_id' => $user->id])
        ->first();
        if(!$order){
            return 'unauthorized';
        }

        OrderDetail::where(['order_id'=>$request->order_id,'supplier_id' => $user->id])
            ->update(['item_status'=>'cancelled']);
        $orderDetails  = OrderDetail::where(['order_id'=>$request->order_id])->with('user')->get();
        $totalCount = $orderDetails->count();
        $cancel = 0;
        $confirmed = 0;
        $shipped = 0;
        foreach ($orderDetails as $orderDetail){
            if ($orderDetail->item_status =='cancelled'){
                $cancel++;
            }
            if ($orderDetail->item_status =='confirmed'){
                $confirmed++;
            }
            if ($orderDetail->item_status =='shipped'){
                $confirmed++;
                $shipped++;
            }
        }
        if ($totalCount == $cancel){
            Order::where('id',$request->order_id)->update(['order_status' =>'cancelled']);
            $title = ['en' => 'Order Cancelled', 'ar' => 'تم الغاء الأمر او الطلب'];
            $description = ['en' => 'Your order is cancel', 'ar' => 'تم إلغاء طلبك'];
            $extras = ['order_id' => $request->order_id];
            createNotification($orderDetails[0]->user, $user->id, 'order_cancelled', $title, $description,$extras);
        }
        else if ($totalCount == $cancel + $confirmed){
            if($shipped > 0){

                Order::where('id',$request->order_id)->update(['order_status' =>'in-pregress']);
                $title = ['en' => 'Order in-progress', 'ar' => 'تم الغاء الأمر او الطلب'];
                $description = ['en' => 'Your order is in-progress state', 'ar' => 'تم إلغاء طلبك'];
                $extras = ['order_id' => $request->order_id];
                createNotification($orderDetails[0]->user, $user->id, 'order_in_progress', $title, $description,$extras);    
            }
            else{
                
                Order::where('id',$request->order_id)->update(['order_status' =>'confirmed']);
                $title = ['en' => 'Order Confirmed', 'ar' => 'تم الغاء الأمر او الطلب'];
                $description = ['en' => 'Your order is confirmed', 'ar' => 'تم إلغاء طلبك'];
                $extras = ['order_id' => $request->order_id];
                createNotification($orderDetails[0]->user, $user->id, 'order_confirmed', $title, $description,$extras);
            }
            
        }
        
        return true;
    }


    public function confirmOrder($request, $fromWeb = false){
        if ($fromWeb){
            $user = $this->user;
        }
        else{
            $user = \request('jwt.user', new \stdClass());
        }
        if($user->is_supplier != 1){
            return 'unauthorized';
        }
        OrderDetail::where(['order_id'=>$request->order_id,'supplier_id' => $user->id, 'item_status' => 'pending'])
            ->update(['item_status'=>'confirmed']);
        $orderDetails  = OrderDetail::where(['order_id'=>$request->order_id])->with('user')->get();
        $status = '';
        foreach ($orderDetails as $orderDetail){
            if ($orderDetail->item_status =='confirmed' || $orderDetail->item_status =='cancelled'){
                $status = 'confirmed';
            }
            else{
                $status = " ";
                break;
            }
        }
        if ($status== 'confirmed'){
            Order::where('id',$request->order_id)->update(['order_status' =>'confirmed']);
            $title = ['en' => 'Order Confirmed', 'ar' => 'تم الغاء الأمر او الطلب'];
            $description = ['en' => 'Your order is placed', 'ar' => 'تم إلغاء طلبك'];
            $extras = ['order_id' => $request->order_id];
            createNotification($orderDetails[0]->user, $user->id, 'order_confirmed', $title, $description,$extras);
        }
        
        return true;
    }



    public function shippedOrder($request, $fromWeb = false){
        if ($fromWeb){
            $user = $this->user;
        }
        else{
            $user = \request('jwt.user', new \stdClass());
        }
        if($user->is_supplier != 1){
            return 'unauthorized';
        }
        $order =  OrderDetail::where(['order_id'=>$request->order_id, 'supplier_id' => $user->id])
        ->first();
        if(!$order){
            return 'unauthorized';
        }
        OrderDetail::where(['order_id'=>$request->order_id,'supplier_id' => $user->id])
            ->update(['item_status'=>'shipped']);
        $orderDetails  = OrderDetail::where(['order_id'=>$request->order_id])->with('user')->get();
        $status = '';
        foreach ($orderDetails as $orderDetail){
            if ($orderDetail->item_status =='shipped'){
                $status = 'shipped';
            }
        }
        if ($status== 'shipped'){
            Order::where('id',$request->order_id)->update(['order_status' =>'in-progress']);
        }
        $title = ['en' => 'Order In-Progress', 'ar' => 'ترتيب قيد التقدم'];
        $description = ['en' => 'Your order is now in-progress state', 'ar' => 'طلبك الآن في حالة التقدم'];
        $this->createNotification($orderDetails[0]->user, 'user', $title, $description, '');
        return true;
    }

    public function deliverOrder($request, $fromWeb = false){
        if ($fromWeb){
            $user = $this->user;
        }
        else{
            $user = \request('jwt.user', new \stdClass());
        }
        if($user->is_supplier != 1){
            return 'unauthorized';
        }
        $order =  OrderDetail::where(['order_id'=>$request->order_id, 'supplier_id' => $user->id])
        ->first();
        if(!$order){
            return 'unauthorized';
        }
        OrderDetail::where(['order_id'=>$request->order_id, 'supplier_id' => $user->id])
            ->update(['item_status'=>'delivered']);
        $orderDetails  = OrderDetail::where(['order_id'=>$request->order_id])->with('user')->get();
        $status = '';
        foreach ($orderDetails as $orderDetail){
            if ($orderDetail->item_status =='delivered'){
                $status = 'delivered';
            }
            else if($orderDetail->item_status =='cancelled')
            {
                continue;
            }
            else{
                $status = "";
                break;
            }
        }
        if ($status== 'delivered'){
            Order::where('id',$request->order_id)->update(['order_status' =>'delivered']);
            $title = ['en' => 'Order Completed', 'ar' => 'تم اكتمال الطلب'];
            $description = ['en' => 'Your order is completed', 'ar' => 'اكتمال طلبك'];
            $this->createNotification($orderDetails[0]->user, 'user', $title, $description,'');
        }
        return true;
    }

    public function reviewOrderDetail($request,$fromWeb = false){
        if ($fromWeb){
            $user = $this->user;
        }
        else{
            $user = \request('jwt.user', new \stdClass());
        }
        if($user->is_supplier == 1)
        {
            return 'unauthorized';
        }
        $order = OrderDetail::where(['id'=> $request->order_detail_id,'rating' =>0,'item_status' => 'delivered', 'user_id' => $user->id])->
            update(['rating' => $request->rating,'review'=>$request->review, 'is_review' => 1]);
        if(!$order)
        {
            return false;
        }
        return $order;

    }

    public function createNotification($user, $type='user', array $title = ['en' => '', 'ar'=> ''], array $description=['en' => '', 'ar'=> ''], $extras='')
    {
        if ($user->is_notification == 1) {
            Notification::create([
                'user_id' => $user->id,
                'fcm_token' => $user->fcm_token,
                'type' => $type,
                'title->en' => $title['en'],
                'title->ar' => $title['ar'],
                'description->en' => $description['en'],
                'description->ar' => $description['ar'],
                'is_seen' => 0,
                'is_read' => 0,
                'extras' => $extras
            ]);
        }
    }


    public function updateCheckOutAddress($request,$fromWeb = false)
    {
        if ($fromWeb) {
            $user = $this->user;
        } else {
            $user = \request('jwt.user', new \stdClass());
        }
        
        $data = $request->only('first_name', 'last_name', 'email', 'user_phone', 'address', 'street_address', 'post_code');
        $data['user_id'] = $user->id;
        $data['type'] = 'billing';
        $billing = Address::updateOrCreate(['user_id' => $data['user_id'], 'type' => 'billing'], $data);
        $shipping = new Address();
        $shipping->id = 0;
        if ($request->shipping_to != '') {
            $data['first_name'] = $request->shipping_first_name;
            $data['last_name'] = $request->shipping_last_name;
            $data['email'] = $request->shipping_email;
            $data['user_phone'] = $request->shipping_user_phone;
            $data['address'] = $request->shipping_address;
            $data['street_address'] = $request->shipping_street_address;
            $data['post_code'] = $request->shipping_post_code;
            $data['type'] = 'shipping';
            $shipping = Address::updateOrCreate(['user_id' => $data['user_id'], 'type' => 'shipping'], $data);

        }
        $cart =  Cart::where('user_id',$user->id)->get();
        $subTotal = 0;
        foreach ($cart as $key => $value) {
            $subTotal += $value->total_price;
        }
        $address['billing'] = $billing;
        $address['shipping'] = $shipping;
        $address['sub_total'] = $subTotal;
        return $address;


    }

    

    public function couponRemoveTrait($request, $fromWeb = false)
    {
        if($fromWeb){
            $user = $this->user;
        }
        else{
            $user = \request('jwt.user', new \stdClass());
        } 
        $check_valid_user = Order::where(['id' => $request->order_id, 'user_id' => $user->id])->first();
        if(!$check_valid_user){
            return "unauthorized";
        }
        $order = Order::where('order_status', '!=', 'delivered')->where('id', $request->order_id)->where('coupon_code', '!=', NULL)->first();
        if($order){
            Coupon::where(['code' => $order->coupon_code, 'percent' => $order->coupon_percent])->update(['status' => 'pending']);
            $order = $order->update(['coupon_code' => NULL,  'coupon_percent' => NULL]);
            if($order){
                return "success";
            }
        }
        return false;
    }

}