<?php


namespace App\Traits;


use App\Models\{Product,Attribute,Coupon,User};
use App\Models\Cart;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;

trait CartTrait
{

    public function addToCart($request){
        $user = $this->user;

        if($user->is_supplier == 1){
            return "supplier";
        }

        $product = Product::find($request->product_id);
        if ($product->quantity == 0 ){
            return "out of stock";
        }
        if ($request->quantity > $product->quantity ){
            return "greater than stock";
        }
        $quantity = $product->quantity - $request->quantity;
        $data['product_price'] = $product->price;
        $data['quantity'] = $request->quantity;
        $data['total_price'] = $data['product_price']*$data['quantity'];
        $data['user_id'] =$user->id;
        $data['supplier_id'] = $product->user_id;
        $data['product_id'] = $request->product_id;
        $cartProductExist = Cart::where('user_id',$user->id)->where('product_id',$request->product_id)->first();
        if ($cartProductExist){
            $productQuantity = $cartProductExist->quantity + $request->quantity;
            $productPrice = $cartProductExist->total_price + $data['total_price'];
            $cartProductExist->update(['quantity' => $productQuantity, 'total_price' => $productPrice]);
            $product->update(['quantity' => $quantity]);
            return "success";
        }
        Cart::create($data);
        $product->update(['quantity' => $quantity]);
        return "success";
    }

    public function updateCartTrait($request,$fromWeb = false){
        if ($fromWeb){
            $user = $this->user;
        }
        else{
            $user = \request('jwt.user', new \stdClass());
        }
        $product_quantity = $request->quantity;

        $cart =  Cart::with('product')->where('user_id',$user->id)->where('id',$request->cart_id)->first();
        if(!$cart){
            return "unauthorized";
        }
        if($product_quantity > $cart->quantity)
        {
            $productQuantity = $product_quantity -  $cart->quantity;
            $productQuantity = $cart->product->quantity - $productQuantity;
            if($productQuantity < 0)
            {
                return "greater than stock";
            }
        }
        else if($product_quantity < $cart->quantity){
            $productQuantity = $cart->quantity - $product_quantity;
            $productQuantity = $productQuantity + $cart->product->quantity;
        }
        else{
            return "please change quantity";
        }
        $total_price = $cart->product->price;
        if ($cart->product->discount > 0){
            $amount = $cart->product->price*$cart->product->discount/100;
            $total_price = $cart->product->price-$amount;
        }
        $total_price = $total_price*$product_quantity;
        Product::where('id',$cart->product->id)->update(['quantity' => $productQuantity]);
        Cart::where('id',$cart->id)->update(['quantity' =>$product_quantity,'total_price' => $total_price ]);
        return "success";
        
    }

    public  function deleteCartTrait($cart_id,$fromWeb = false){

        if ($fromWeb){
            $user = $this->user;
        }
        else{
            $user = \request('jwt.user', new \stdClass());
        }
        $cart =  Cart::where('user_id',$user->id)->where('id',$cart_id)->first();
        if(!$cart){
            return false;
        }
        $cart->product->increment('quantity', $cart->quantity);
        $cart->delete();
        $cartcount = Cart::where('user_id', $user->id)->count();
        if($cartcount == 0){
            $user = User::where('id', $user->id)->select('id', 'coupon_id')->first();
            User::where('id', $user->id)->update(['coupon_id' => NULL]);

        }
       return 'success';

    }

    public  function cartIndex(){
        $user = $this->user;
        return $this->getCartData($user);;
    }

    public function getCartData($user){
        $selectProduct= function ($query){
            $query->select('id','title', 'image');
        };
        $selectUser = function ($query){
            $query->select('id');
        };
        $cartData = Cart::select('id','user_id','product_id','quantity','product_price','total_price')->with(['product'=> $selectProduct,'user'=>$selectUser])->where('user_id',$user->id)->get();
        $subTotal = 0;
        if (count($cartData)>0){
            foreach($cartData as $key => $value){
                $subTotal+= $value['total_price'];
            }
            
        }
        $carts['data'] = $cartData;
        $carts['sub_total'] = $subTotal;
        return $carts;
    }

}