<?php
namespace App\Traits;
use App\Http\Libraries\Uploader;
use App\Models\Conversation;
use App\Models\Message;
use App\Models\{Product,User};
use Carbon\Carbon;

trait ChatTrait

{

   public  function getConversationTrait($fromWeb = false){
       if ($fromWeb){
           $user = $this->user;
       }
       else{
           $user = \request('jwt.user', new \stdClass());
       }
       $conversations = Conversation::whereHas('conversationUsers', function ($query) use($user){
           return $query->where('user_id', $user->id);
       })->with([
               'product'=>function($query){
                   $query->select('id','name','price',\DB::raw('concat("' . url('/') . '/",image) as image'));
               },
               'conversationUsers' => function($query) {
           return $query->select('id','first_name','last_name','supplier_name','image');
           },
               'latestMessage'
           ])
           ->select('id','created_at','product_id')
           ->get();
       foreach($conversations as $conversation)
       {
           $conversation->latestMessage->time  = Carbon::parse($conversation->latestMessage->created_at)->diffForHumans();
           $conversation->product->price = getPriceObject($conversation->product->price);
       }
       return $conversations;
   }

   public function startConversationTrait($request , $fromWeb = false){
       if ($fromWeb){
           $user = $this->user;
       }
       else{
           $user = \request('jwt.user', new \stdClass());
       }
       $product = Product::findOrFail($request->product_id);
       $store_id = $product->user_id;
       $users = [$store_id, $user->id];

       $conversation = Conversation::whereHas('conversationUsers', function($query) use($store_id){
           return $query->where('id', $store_id);
       })->whereHas('conversationUsers', function($query) use($user){
           return $query->where('id', $user->id);
       })->where('product_id', $product->id)
           ->first();

       if (!$conversation)
       {
           $conversation = Conversation::create(['product_id' => $product->id]);
           $conversation->conversationUsers()->sync($users);
       }

       $conversation = Conversation::with([
           'product'=>function($query){
           $query->select('id','title','price',\DB::raw('concat("' . url('/') . '/",image)as image'));
           },
           'conversationUsers' => function($query){
           return $query->select('id','first_name','last_name','supplier_name','image');
           },
           'messages'
       ])
           ->where('id', $conversation->id)
           ->select('id','created_at','product_id')
           ->first();
       $conversation->product->price = getPriceObject($conversation->product->price);
       return $conversation;
   }

   public function sendMessageTrait($request ,$fromWeb = false){
       if ($fromWeb){
           $user = $this->user;
       }
       else{
           $user = \request('jwt.user', new \stdClass());
       }
       $data = $request->only('conversation_id', 'message_type');
       $data['sent_by'] = $user->id;
       if($request->message_type == 'text')
       {
           $data['message'] = $request->message;
       }
       else{
           if ($request->file('file')){
               $uploader = new Uploader();
               $uploader->setInputName('file');
               if ($uploader->isValidFile()) {
                   $uploader->upload('messages', $uploader->fileName);
                   if ($uploader->isUploaded()) {
                       $data['file'] = $uploader->getUploadedPath();
                   }
               }
               if (!$uploader->isUploaded()) {
                   return responseBuilder()->error(__('something went wrong'));
               }
           }
       }
       $message = Message::create($data);
       $message->time = carbon::parse($message->created_at)->diffForHumans();
       return $message;
   }

   public function getMessagesTrait($request,$fromWeb= false){
       $messages = Message::orderBy('created_at', 'desc')->where('conversation_id', $request->conversation_id)->select('id','conversation_id','message_type','message','file','sent_by','created_at')->skip($request->skip)->take(20)->get();
       $messages = $messages->reverse();
       $masterArray = [];
       foreach($messages as $message)
       {
           $message->time = Carbon::parse($message->created_at)->diffForHumans();
           $masterArray[] = $message;
       }
       return $masterArray;
   }

   public function conversationDeleteTrait($id, $fromWeb = false)
   {
        if ($fromWeb){
            $user = $this->user;
        }
        else{
            $user = \request('jwt.user', new \stdClass());
        }

        $conversation = Conversation::where('id', $id)->whereHas('conversationUsers', function($query) use($user) {
             $query->where('user_id', $user->id);
        })->firstOrFail();

        $deleted_by = "user1_deleted";
        if($user->is_supplier == 1){
            $deleted_by = "user2_deleted";
        }
        Conversation::where('id', $id)->update([$deleted_by => $user->id]);

        $conversation->delete();
        return true;
   }

}