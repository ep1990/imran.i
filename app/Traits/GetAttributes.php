<?php
/**
 * Created by PhpStorm.
 * User: hp
 * Date: 1/7/2019
 * Time: 6:51 PM
 */

namespace App\Traits;
use App\Models\Agency;
use App\Models\Attribute;
use App\Models\Category;
use App\Models\Country;
use App\Models\Language;
use function Couchbase\defaultDecoder;
use Illuminate\Support\Arr;

use App\Models\Admin;
use App\Models\Brand;
use App\Models\BrandLanguage;
use App\Models\CarModel;
use App\Models\CarModelLanguage;
use App\Models\ClaimReason;
use App\Models\DeliveryType;
use App\Models\DeliveryTypeLanguage;
use App\Models\Label;
use App\Models\LanguageLabel;
use App\Models\LanguagePaymentTerm;
use App\Models\LanguageSpeedIndex;
use App\Models\LanguageVehicle;
use App\Models\Order;
use App\Models\OrderDetail;
use App\Models\Product;
use App\Models\SpeedIndex;
use App\Models\Vehicle;
use Carbon\Carbon;
use function MongoDB\BSON\toJSON;

trait GetAttributes
{
  use Translations,EMails;
  public function getCarModels()
  {
//      $carModels = CarModelLanguage::whereHas('carModel')->pluck('title', 'car_model_id');
////      dd($carModels);
////      $carModels->prepend('Select Car Model', 0);
////      return $carModels;
      $carModels = CarModel::with('languages')->get();
      $this->setTranslations($carModels);
      foreach ($carModels as $key => $carMod) {
          $carModelsData[$carMod->id] = $carMod->translation->title;
      }
      $carModelsData = arr::prepend($carModelsData, 'Select Car Model', 0);
      return $carModelsData;

  }
    public function getVehicles()
    {
//        $vehicles=LanguageVehicle::whereHas('vehicle')->pluck('title','vehicle_id');
//        $vehicles->prepend('Select Vehicle Type', 0);
//        return $vehicles;
        $vehicles = Vehicle::with('languages')->get();
        $this->setTranslations($vehicles);
        foreach ($vehicles as $key => $vehicle) {
            $vehicleData[$vehicle->id] = $vehicle->translation->title;
        }
        $vehicleData = arr::prepend($vehicleData, 'Select Vehicles', 0);
        return $vehicleData;
    }
    public function getSpeedIndexes()
    {
        $speedIndexes = SpeedIndex::with('languages')->get();
        $this->setTranslations($speedIndexes);
        foreach ($speedIndexes as $key => $speedIndex) {
            $speedIndexesData[$speedIndex->id] = $speedIndex->translation->title;
        }
        $speedIndexesData = arr::prepend($speedIndexesData, 'Select Speed Index', 0);
        return $speedIndexesData;
    }
    public function getLabels()
    {
//        $labels=LanguageLabel::whereHas('label')->pluck('title','label_id');
//        $labels->prepend('Select Label', 0);
//        return $labels;
        $labels = Label::with('languages')->get();
        $this->setTranslations($labels);
        foreach ($labels as $key => $label) {
            $labelData[$label->id] = $label->translation->title;
        }
        $labelData = arr::prepend($labelData, 'Select Label ', 0);
        return $labelData;


    }
    public function getBrands()
    {
//        $brands =BrandLanguage::whereHas('brand')->pluck('title','brand_id');
//        $brands->prepend('Select Brand', 0);
//        return $brands;

        $brands = Brand::with('languages')->get();
        $this->setTranslations($brands);
        foreach ($brands as $key => $brand) {
            $brandData[$brand->id] = $brand->translation->title;
        }
       $brandData = arr::prepend($brandData, 'Select Brand ', 0);
        return $brandData;
    }
    public function getSelectedLabels($productId)
    {
        $selectedData=[];
        if($productId!=0) {
            $selectedLabels = Product::with('labels.languages')
                ->where('id', '=', $productId)
                ->get();
            if (count($selectedLabels) != 0) {
                foreach ($selectedLabels[0]->labels as $key => $labels) {
                    foreach ($labels->languages as $labelLanuguage) {
                        $selectedData[$key] = $labelLanuguage->pivot->label_id;
                    }
                }
            }
        }
        return $selectedData;
    }
    public function getSeasons()
    {
        $seasons=[];
        $seasons=[''=>'Select Season',
            'unspecified'=>__('Unspecified'),
            'winter'=>__('winter'),
            'summer'=>__('summer'),
            'all-seasons'=>__('All-seasons'),
            'summer&all-seasons'=>__('Summer & All-seasons'),
            'winter&all-seasons'=>__('Winter & All-seasons')
        ];
        return $seasons;
    }
    public function getTyreTypes()
    {
        $tireTypes=[];
        $tireTypes=[''=>'Select Tyre Type','unspecified'=>'Unspecified','runflat'=>'runflat'];
        return $tireTypes;
    }
    public function vatTax($price)
    {

        $tax=config('settings.value_added_tax');
        $valueAddedTax=0;
        if($tax!=0) {
            $valueAddedTax = ($tax / 100) * $price;
        }
        return $valueAddedTax;
    }
    public function dgTax($price,$dg)
    {
        $dgTax=0;
        if($dg!=0) {
            $dgTax = ($dg / 100) * $price;
        }
        return $dgTax;
    }
    public function discount($discount,$price)
    {
        $discountAmount=0;
        if($discount!=0) {
            $discountAmount = ($discount / 100) * $price;
        }
        return $discountAmount;
    }
    public function deliveryTypes()
    {
//        $deliveryTypes=DeliveryTypeLanguage::whereHas('deliveryType')->pluck('title','delivery_type_id');
//        $deliveryTypes->prepend('Select Delivery Type', 0);
//        return $deliveryTypes;

        $deliveryTypes = DeliveryType::with('languages')->get();
        $this->setTranslations($deliveryTypes);
        foreach ($deliveryTypes as $key => $deliveryType) {
            $deliveryTypeData[$deliveryType->id] = $deliveryType->translation->title;
        }
        $deliveryTypeData = arr::prepend($deliveryTypeData, 'Select Delivery Type ', 0);
        return $deliveryTypeData;
    }
    public function getRegions()
    {
        $regions=[];
        $regions=[''=>'Select Region','EU'=>'EU','E'=>'E'];
        return $regions;
    }
   public function getTrashedProducts()
    {
        $products=function($products)
        {
            $products->withTrashed();
            $products->with('languages');
            $products->whereHas('languages');
        };
        return $products;
    }
    public function getTrashedBrands()
    {
        $brands=function ($brands)
        {
            $brands->withTrashed();
            $brands->with('languages');
            $brands->whereHas('languages');
        };
        return $brands;
    }
    public function getTrashedDeliveries()
    {
        $deliveryType=function ($delivery)
        {
            $delivery->withTrashed();
            $delivery->with('languages');
            $delivery->whereHas('languages');
        };
        return $deliveryType;
    }
  public function getClaimReason()
  {
      $claims=ClaimReason::with('children','languages')
          ->where('parent_id','=',null)
          ->get();
      $this->setTranslations($claims,'languages',['children'=>'languages']);
      return $claims;
  }
  public function getPaymentTerms()
  {
      $paymentTerms=LanguagePaymentTerm::whereHas('paymentTerm')->pluck('title','payment_term_id');
      $paymentTerms->prepend('Select Payment Type', 0);
      return $paymentTerms;

  }
  public function getOrderDetails($orderId)
  {

      $products=function($products)
      {
          $products->withTrashed();
          $products->with('languages');
          $products->whereHas('languages');
      };
      $brands=function ($brands)
      {
          $brands->withTrashed();
          $brands->with('languages');
          $brands->whereHas('languages');
      };
      $deliveryType=function ($delivery)
      {
          $delivery->withTrashed();
          $delivery->with('languages');
          $delivery->whereHas('languages');
      };

      $order=Order::with(['orderDetails','orderDetails.product'=>$products,'orderDetails.brand'=>$brands,'orderDetails.deliveryType'=>$deliveryType])
          ->whereHas('orderDetails')
          ->where('id','=',$orderId)
          ->first();

           if($order!=null) {
               $this->setTranslations($order->orderDetails, '', ['product' => 'languages', 'brand' => 'languages', 'deliveryType' => 'languages']);
           }
      return $order;
  }

  public function getOrderDetailsDetail($orderId,$orderDetailId)
  {

      $delivery=$this->getTrashedDeliveries();
      $orderDetails=OrderDetail::with(['order','deliveryType','product'=>$this->getTrashedProducts(),'brand'=>$this->getTrashedBrands()])
          ->where('id','=',$orderDetailId)
          ->firstOrFail();

      $orderDetails->deliveryType->translation=$this->translateRelation( $orderDetails->deliveryType);
      $orderDetails->product->translation=$this->translateRelation( $orderDetails->product);

      $orderDetails->brand->translation=$this->translateRelation( $orderDetails->brand);

      return $orderDetails;
  }

  public function invoice($orderId)
  {
      $deliveryType=function ($delivery)
      {
          $delivery->withTrashed();
          $delivery->with('languages');
          $delivery->whereHas('languages');
      };
      $products=function($products) use($deliveryType)
      {
          $products->with(['deliveryType'=>$deliveryType]);
          $products->withTrashed();
          $products->with('languages');
          $products->whereHas('languages');
      };
      $brands=function ($brands)
      {
          $brands->withTrashed();
          $brands->with('languages');
          $brands->whereHas('languages');
      };

      $order=Order::with(['orderDetails','admin','orderDetails.product'=>$products,'orderDetails.brand'=>$brands])
          ->whereHas('orderDetails')
          ->where('id','=',$orderId)
          ->first();
//      $order->deliveryType->translation=$this->translateRelation($order->deliveryType);
      $this->setTranslations($order->orderDetails , '' , ['product'=>'languages','brand'=>'languages']);
      foreach($order->orderDetails as $key=>$orderDetail) {
              $order->orderDetails[$key]->product->deliveryType->translation=$this->translateRelation($orderDetail->product->deliveryType);
      }

      return $order;
  }

 public function getAdmins()
  {
      $admin=Admin::all()->pluck('full_name','id')->toArray();
      $admin= arr::prepend($admin,'Select  Customer','');
      return $admin;
  }

  public function changeOrderStatusData($orderId,$status)
  {
      $current_time = Carbon::now()->timestamp;
      $routeData=null;

      if($status=='complete')
      {
          $data['order_status']=$status;
          $data['complete_date']=$current_time;
          $order = Order::updateOrCreate(['id' => $orderId], $data);
          $routeData='deliver';
          $admin=Admin::where('id','=',$order->admin_id)->first();
          $amount=$admin->credit_limit+$order->total_amount;
          Admin::where('id','=',$order->admin_id)->update(['credit_limit'=>$amount]);
          $data['receiver_email'] = $admin->email;
          $data['receiver_name'] = $admin->full_name;
          $data['message_text']='Order Complete';
          $data['full_name'] = $admin->full_name;
          $data['sender_name'] = config('settings.company_name');
          $data['subject'] = 'Order Complete';
          $data['email']=config('settings.email');
          $data['orderDetailsHeading']='Order Details ';
          $data['check']=0;
          $data['order_id']=$orderId;
          $data['status']=' Order Number '.$orderId.' Complete';
          $data['order']=null;
          $this->sendMail($data, 'emails.contact_us', $data['subject'], $data['receiver_email'], $data['email']);

      }
      else if($status=='delivered')
      {
          $data['delivery_date']=$current_time;
          $data['order_status']=$status;
          $order = Order::updateOrCreate(['id' => $orderId], $data);
          $routeData='order';
          $admin=Admin::where('id','=',$order->admin_id)->first();
          $data['receiver_email'] = $admin->email;
          $data['receiver_name'] = $admin->full_name;
          $data['message_text']='Order Delivered';
          $data['full_name'] = $admin->full_name;
          $data['sender_name'] = config('settings.company_name');
          $data['subject'] = 'Order Delivered';
          $data['email']=config('settings.email');
          $data['orderDetailsHeading']='Order Details ';
          $data['check']=0;
          $data['order_id']=$orderId;
          $data['status']=' Order Number '.$orderId.' Delivered';
          $data['order']=null;
          $this->sendMail($data, 'emails.contact_us', $data['subject'], $data['receiver_email'], $data['email']);
      }
      else if($status=='order')
      {
          $data['delivery_date']=$current_time;
          $data['order_status']=$status;
          $order = Order::updateOrCreate(['id' => $orderId], $data);

      }

      return  $routeData;
  }

//    public function getCategories()
//    {
//        $categories = Category::all()->pluck('parent_id','name');
//        $categoryData = arr::prepend($categories, 'Select Brand ', 0);
//        return $categoryData;
//    }
    public function getCategories()
    {
        $categories = Category::where('parent_id','=',0)->get();
        $categoryData = [];
        if(count($categories) > 0){
            foreach ($categories as $key => $category) {
                $categoryData[$category->id] = $category->title['en'];
            }
        }
        $categoryData = arr::prepend($categoryData, 'Select Category ', 0);
        return $categoryData;
    }
    public function getSubCategories($id)
    {
        $categories = Category::where('parent_id',$id)->get();
        $categoryData= [];
        foreach ($categories as $key => $category) {
            $categoryData[$category->id] = $category->title['en'];
        }
//        $categoryData = arr::prepend($categoryData, 'Select sub category ', 0);
        return $categoryData;
    }

    public function getAttributes()
    {
        $categoryData[] = '';
        $attributes = Attribute::where(['parent_id' => 0])->get();
        if (count($attributes)>0){

            foreach ($attributes as $key => $attribute) {
                $categoryData[$attribute->id] = $attribute->title['en'];
            }
        }
        $categoryData = arr::prepend($categoryData, 'Select Attribute ', 0);

        return $categoryData;
    }
    public function getCategoryAttributes($id){
//      $checkAttributes = function ($attribute){
//          $attribute->with('languages');
//      };
      $attributeRelations = function($attribute){
          $attribute->with('subAttributes');
      };
        $attributesData=[];
        $category = Category::whereHas('attributes')->with(['attributes'=> $attributeRelations])->where('id',$id)->first();
       if ($category == null){
           return $attributesData;
       }
     $attributes =  $category->attributes;
//       foreach ($attributes as $key => $attribute){
//           foreach($attribute->subAttributes as $key2 =>$subAttribute ){
//               $subAttribute->translation = $this->translateRelation($subAttribute);
//               unset($subAttribute->languages);
//           }
//       }

        if ($attributes !== null){
         foreach ($attributes as $key => $attribute) {
             $temp = new \stdClass();
             $temp->id = $attribute->id;
             $temp->title = $attribute->title['en'];
             $temp->subAttributes = [];
                 foreach($attribute->subAttributes as $subAttributes => $subAttribute){
                     $temp->subAttributes[$subAttribute->id] = $subAttribute->title['en'];

                 }


             $attributesData[] = $temp;
         }
     }

        return $attributesData;
    }

    public function getsubAttriutes($array){
          $attributes = Attribute::with('subAttributes')->whereIn('id',$array)->get();
        return $attributes;

    }
    public function getCountryCities($country_id)
    {
        $cities = Country::whereHas('languages')->with('languages')->where('parent_id',$country_id)->get();
        $this->setTranslations($cities);
        if(count($cities) > 0){
            foreach ($cities as $key => $city) {
                $citiesData[$city->id] = $city->translation->name;
            }
        }
        return $citiesData;
    }
    public function getCountries()
    {
        $countries = Country::whereHas('languages')->with('languages')->where('parent_id',0)->get();
        $this->setTranslations($countries);
        $countriesData =[];
        if(count($countries) > 0){
            foreach ($countries as $key => $country) {
                $countriesData[$country->id] = $country->translation->name;
            }
        }
//        $countriesData = arr::prepend($countriesData, 'Select Country ', 0);
        return $countriesData;
    }

    public function getAgencies(){
        $agencies = Agency::whereHas('languages')->with('languages')->where('parent_id',0)->get();
        $this->setTranslations($agencies);
        $agenciesData =[];
        if(count($agencies) > 0){
            foreach ($agencies as $key => $agency) {
                $agenciesData[$agency->id] = $agency->translation->name;
            }
        }
//        $countriesData = arr::prepend($countriesData, 'Select Country ', 0);
        return $agenciesData;
    }

    public function getAgenciesBranhches($id){
        $branches = Agency::select('id')->whereHas('languages')->with('languages')->where('parent_id', $id)->get();

        $this->setTranslations($branches);
        $branchesData =[];
        if(count($branches) > 0){
            foreach ($branches as $key => $branch) {
                $branchesData[$branch->id] = $branch->translation->name;
            }
        }
        return $branchesData;
    }

}