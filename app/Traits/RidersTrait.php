<?php


namespace App\Traits;


use App\Models\Notification;
use App\Models\Order;
use App\Models\OrderDetail;
use App\Models\RiderOrder;
use App\Models\User;
use http\Env\Request;
use Illuminate\Support\Facades\DB;
use function foo\func;
use function GuzzleHttp\Promise\all;

trait RidersTrait
{

    public function getAvailableRiders(){
        $riders = User::select('id','first_name','last_name')->
        where('type','=','rider')
            ->where('available','=','0')
            ->get();

        return $riders;

    }

    public function createRidersOrder($request,$fromWeb = false){
        if ($fromWeb){
            $user = $this->user;
        }
        else{
            $user = \request('jwt.user', new \stdClass());
            $token = \request('jwt.token', new \stdClass());
        }
        if (!$user->id == $request->chef_id){
            return false;
        }
        $data=[];
        foreach($request->order_detail_ids as $key => $value){
            $data[]= ['order_detail_id'=> $value,'chef_id'=>$request->chef_id,'rider_id' => $request->rider_id,'order_id' => $request->order_id];
        }

         RiderOrder::insert($data);
        $this->createNotification($request->rider_id,'New Order',
            'You Have Received New Order',$request->order_id,'New Order','You Have Received New Order',$user->user_image);
        OrderDetail::where(['store_id'=>$request->chef_id,'order_id' => $request->order_id])->update(['rider'=>1,'rider_id' => $request->rider_id]);
        Order::where('id' ,$request->order_id)->update(['order_status'=>'in-progress']);
        return true;
    }

    public function riderOrders($request,$fromWeb = false){
        if ($fromWeb){
            $user = $this->user;
        }
        else{
            $user = \request('jwt.user', new \stdClass());
            $token = \request('jwt.token', new \stdClass());
        }
        if ($request->has('status')){
            $where = ['rider_id'=> $user->id,'status' => $request->status];
        }else{
            $where = ['rider_id'=> $user->id];
        }
        $checkRider =function ($order) use($user){
            $order->where('rider_id',$user->id);
        };
        $selectRider = function($query) use ($where){
            $query->select('id','rider_id','chef_id','order_detail_id','order_id','status')
                ->with(['orderDetails:id,quantity,total_price,image'])
                ->where($where);
        };
        $orders = Order::select('id','order_number')->whereHas('riders',$checkRider)->with(['riders'=> $selectRider])->get();
        foreach ($orders as $key => $order){
            $totalAmount = 0;
            $order->total_products = count($order->riders);
            if($order->total_products > 0){
                $order->status = $order->riders[0]->status;
                $order->image = $order->riders[0]->orderDetails->image;
            }
            foreach($order->riders as $key => $rider){
                $totalAmount +=  $rider->orderDetails->total_price;
                $order->store_id = $rider->chef_id;
            }
            $order->total_price = getPriceObject($totalAmount);
            unset($order->riders);
        }
        return $orders;
    }

    public function riderOrderDetails($request,$fromWeb = false){

        if ($fromWeb){
            $user = $this->user;
        }
        else{
            $user = \request('jwt.user', new \stdClass());
            $token = \request('jwt.token', new \stdClass());
        }
        $checkOrderDetail = function($query) use ($request, $user){
          $query->where(['order_id'=> $request->order_id,'rider_id'=>$user->id]);
        };
        $selectOrderDetails = function ($query){
            $query->select('id','order_id','item_status','product_id','quantity','total_price','image','created_at')
            ->with(['product'=> function($product){
                $product->select('id','service_fee','delivery_fee','name','meal_id');
            }]);
        };
        $pending = function ($pending){
            $pending->where('item_status','pending');
        };
        $shipping = function ($pending){
            $pending->where('item_status','shipping');
        };
        $canceled = function ($canceled){
            $canceled->where('item_status','canceled');
        };
        $complete= function ($complete){
            $complete->where('item_status','complete');
        };
        $order = Order::select('id','order_number','shipping_address','billing_address','created_at','payment_method','order_note')
            ->whereHas('orderDetails',$checkOrderDetail)
            ->with(['orderDetails' => $selectOrderDetails])
            ->withCount(['orderDetails','orderDetails as pending' => $pending,'orderDetails as shipping' => $shipping,'orderDetails as canceled' => $canceled,'orderDetails as completed' => $complete])
            ->firstOrFail();
        $order->total_products = $order->order_details_count;
        unset($order->order_details_count);
        $total = 0;
        $total_shipping = 0;
        $status = 'canceled';
        foreach ($order->orderDetails as $key => $orderDetail){
            $total += $orderDetail->total_price;
            $total_shipping += $orderDetail->product['delivery_fee'] + $orderDetail->product['service_fee'];

            $orderDetail->total_price = getPriceObject($orderDetail->total_price);
            $orderDetail->product->service_fee =getPriceObject($orderDetail->product->service_fee);
            $orderDetail->product->delivery_fee =getPriceObject($orderDetail->product->delivery_fee);
            if ($orderDetail->item_status == 'pending'){
                $status = 'pending';
            }
            if ($orderDetail->item_status == 'shipping'){
                $status = 'accepted';
            }
            if ($orderDetail->item_status == 'delivered'){
                $status = 'delivered';
            }

        }
        $order->total = getPriceObject($total);
        $order->shipping_total = getPriceObject($total_shipping);
        $order->order_status = $status;
        $order->shipping_address = json_decode($order->shipping_address);
        $order->billing_address = json_decode($order->billing_address);
        return $order;
    }

    public function riderAcceptOrder($request,$fromWeb = false){
        if ($fromWeb){
            $user = $this->user;
        }
        else{
            $user = \request('jwt.user', new \stdClass());
            $token = \request('jwt.token', new \stdClass());
        }

        $order = Order::find($request->order_id);
        $order->update(['order_status'=> 'in-progress']);
        if($order){
            RiderOrder::where(['order_id'=>$request->order_id,'rider_id'=>$user->id])->update(['status'=>'accepted']);
            OrderDetail::where(['order_id'=>$request->order_id,'rider_id'=>$user->id])->update(['item_status'=>'shipping']);
            $user->update(['available'=>1]);
            $this->createNotification($request->store_id,'Order Accepted',
                'Rider Accept The Order',$request->order_id,'New Order','Rider Accept The Order',$user->user_image);
        }

        return true;
    }
    public function ignoreOrderTrait($request , $fromWeb= false){
        if ($fromWeb){
            $user = $this->user;
        }
        else{
            $user = \request('jwt.user', new \stdClass());
            $token = \request('jwt.token', new \stdClass());
        }
        $order = Order::find($request->order_id);
        $order->update(['order_status'=> 'pending']);
        if($order){
            RiderOrder::where(['order_id'=>$request->order_id,'rider_id'=>$user->id])->update(['status'=>'canceled']);
            OrderDetail::where(['order_id'=>$request->order_id,'rider_id'=>$user->id])->update(['item_status'=>'pending','rider'=>0,'rider_id' =>NULL]);
            $this->createNotification($request->store_id,'Order Ignored',
                'Rider Ignored The Order',$request->order_id,'Order Ignored','Rider Ignored The Order',$user->user_image);
        }
        return true;
    }

    public function orderDeliveredTrait( $request ,$fromWeb =false){
        if ($fromWeb){
            $user = $this->user;
        }
        else{
            $user = \request('jwt.user', new \stdClass());
            $token = \request('jwt.token', new \stdClass());
        }
            RiderOrder::where(['order_id'=>$request->order_id,'rider_id'=>$user->id])->update(['status'=>'delivered']);
            OrderDetail::where(['order_id'=>$request->order_id,'rider_id'=>$user->id])->update(['item_status'=>'delivered']);
        $this->createNotification($request->store_id,'Order Delivered',
            'Rider Delivered The Order',$request->order_id,'Order Delivered','Rider Delivered The Order',$user->user_image);
            $user->update(['available' => 0]);
            $orderDetails  = OrderDetail::where(['order_id'=>$request->order_id])->get();
            $status = '';
            foreach ($orderDetails as $orderDetail){
                if ($orderDetail->item_status =='delivered'){
                    if ($orderDetail->rider_id == $user->id){
                        $this->createNotification($orderDetail->user_id,'Product Delivered',
                            'Rider Delivered The Product',$orderDetail->order_id,'Product Delivered','Rider Delivered The Product',$orderDetail->image);
                    }
                    $status = 'completed';
                }else{
                    $status = '';
                    exit;
                }
            }
            if ($status== 'completed'){
                OrderDetail::where(['order_id'=>$request->order_id,'item_status' =>'delivered'])
                    ->update(['item_status'=>'complete']);
                Order::where('id',$request->order_id)->update(['order_status' =>'completed']);
                $this->createNotification($orderDetails[0]->user_id,'Order Completed',
                    'Order Completed',$request->order_id,'Order Completed','Order Completed',$orderDetails[0]->image);
            }
            return true;
    }
    public function createNotification($user_id,$title_en,$description_en,$order_id,$title_ar,$description_ar,$image){
        Notification::create(['user_id' => $user_id,'extras->order_id' => $order_id,
            'extras->image' => $image,
            'title->en' => $title_en,
            'title->ar' => $title_ar,
            'description->en' => $description_en,
            'description->ar' => $description_ar,
        ]);
    }
}