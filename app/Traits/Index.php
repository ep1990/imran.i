<?php


namespace App\Traits;


use App\Models\User;

trait Index
{
    public function stores(){
        $stores  = User::select('id','chef_name','address','longitude','latitude','rating',\DB::raw('concat("' . url('/') . '/",user_image)as user_image'))->where('type','=','chef')->paginate(10);
        return  $stores;
    }

    public function storeInfo($store_id){
        $selectProduct = function ($query){
            $query->select('id','slug','meal_id','price','name','description','user_id',\DB::raw('concat("' . url('/') . '/",image)as image'))->
            with(['images'=>function ($query){
                $query->select('product_id','default_image',\DB::raw('concat("' . url('/') . '/",image)as image'));
            }]);
        };
       $store = User::select('id','chef_name','address','longitude','latitude','rating',\DB::raw('concat("' . url('/') . '/",user_image)as user_image'))->with(['products' => $selectProduct,'reviews.user'])->where('type','=','chef')->firstOrFail();
       return   $store;
    }

}