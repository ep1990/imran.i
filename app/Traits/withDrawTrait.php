<?php


namespace App\Traits;


use App\Models\Withdraw;

trait withDrawTrait
{

    public function showWithDraw($fromWeb = false){
        if ($fromWeb){
            $user = $this->user;

        }
        else{
            $user = \request('jwt.user', new \stdClass());
            $token = \request('jwt.token', new \stdClass());
        }
        if ($user->type!='chef'){
            return false;
        }
        $withdraws =  Withdraw::where([
            'user_id' => $user->id,
        ])->orderBy('id', 'DESC')->paginate(20);
        return $withdraws;
    }

    public function saveWithdrawRequest($request ,$fromWeb = false){
        if ($fromWeb){
            $user = $this->user;

        }
        else{
            $user = \request('jwt.user', new \stdClass());
            $token = \request('jwt.token', new \stdClass());
        }
     $withDraw =    Withdraw::create([
            'user_id' => $user->id,
            'amount' => $request->get('amount', 0),
            'additional_details' => $request->get('additional_details', ""),
            'method' => $request->method
        ]);

        return $withDraw;
    }
}