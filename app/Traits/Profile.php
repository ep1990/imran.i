<?php
namespace App\Traits;

use App\Models\{User, Product};

trait Profile{
    public function updateProfileTrait($request)
    {
        $user = Session()->get('USER_DATA');
        if($user->is_supplier == 0){
            $data  =  $request->only('first_name', 'last_name', 'address', 'phone_number', 'gender', 'latitude', 'longitude');
            if($request->has('image')){
                $data['image'] = $request->image;
            }
            $user =  User::updateOrCreate(['id' => auth()->user()->id],$data);
            $user['token'] = session('USER_DATA.token');
            return $user;
        }

        $data  =  $request->only('address', 'phone_number', 'latitude', 'longitude', 'city_id');
        if($request->has("supplier_name_en")){
            $data['supplier_name->en'] = $request->supplier_name_en;
        }

        if($request->has("supplier_name_ar")){
            $data['supplier_name->ar'] = $request->supplier_name_ar;
        }
        if($request->has("description_en")){
            $data['description->en'] = $request->description_en;
        }

        if($request->has("description_ar")){
            $data['description->ar'] = $request->description_ar;
        }
        if($request->has('image')){
            $data['image'] = $request->image;
        }
        
        $user =  User::updateOrCreate(['id' => auth()->user()->id],$data);
        Product::where('user_id', $user->id)->update(['latitude' => $user->latitude, 'longitude' => $user->longitude, 'address' => $user->address, 'city_id' => $user->city_id]);
        $user['token'] = session('USER_DATA.token');
        return $user;


    }
}